<?php
/**
 * API de vérification : vérification de la validité du nombre d'inscrits fourmi
 *
 * @plugin     list_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérifie que l'entier est cohérent c'est à dire supérieurs aux suffrages exprimés et nuls ainsi qu'à la somme des voix receuillies par les listes
 *
 * @param string $valeur
 *   La valeur à vérifier.
 * @param array $options
 *   Si ce tableau associatif contient une valeur pour 'min' ou 'max', il sera substitué au contrôle entre zéro et le nombre de siège à pourvoir.
 * @return string
 *   Retourne une chaine vide si c'est valide, sinon une chaine expliquant l'erreur.
 */
function verifier_list_inscrits_dist($valeur, $options = array()) {
	$erreur = _T('verifier:erreur_entier');
	
	// Pas de tableau ni d'objet
	if (is_numeric($valeur) and $valeur == intval($valeur)) {
		// Si c'est une chaine on convertit en entier
		$valeur = intval($valeur);
		$ok = true;
		$erreur = '';

		if (!isset($options['min'])) {
			$options['min'] = intval(_request('blancs')) + intval(_request('nuls'));
			// calculer la somme des voix de chaque liste
			$id_list_scrutin = intval(_request('id_list_scrutin'));
			if ($id_list_scrutin) {
				if ($tous = sql_allfetsel('id_list_elec', 'spip_list_elecs', 'id_list_scrutin=' . $id_list_scrutin)) {
					foreach ($tous as $liste) {
						$voix = intval(_request('id_list_elec-'.$liste['id_list_elec']));
						$options['min'] = $options['min'] + $voix;
					}
				}
			}
		}

		if (isset($options['min'])) {
			$ok = ($ok and ($valeur >= $options['min']));
		}
		if (isset($options['max'])) {
			$ok = ($ok and ($valeur <= $options['max']));
		}
		if (!$ok) {
			if (isset($options['min']) and isset($options['max'])) {
				$erreur = _T('verifier:erreur_entier_entre', $options);
			} elseif (isset($options['max'])) {
				$erreur = _T('verifier:erreur_entier_max', $options);
			} else {
				$erreur = _T('verifier:erreur_entier_min', $options);
			}
		}
		

	}

	return $erreur;
}
