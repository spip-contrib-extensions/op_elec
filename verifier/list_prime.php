<?php
/**
 * API de vérification : vérification de la validité d'une prime
 *
 * @plugin     list_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérifie que l'entier est cohérent c'est à dire en dessous du nombre de siège
 *
 * @param string $valeur
 *   La valeur à vérifier.
 * @param array $options
 *   Si ce tableau associatif contient une valeur pour 'min' ou 'max', il sera substitué au contrôle entre zéro et le nombre de siège à pourvoir.
 * @return string
 *   Retourne une chaine vide si c'est valide, sinon une chaine expliquant l'erreur.
 */
function verifier_list_prime_dist($valeur, $options = array()) {
	$erreur = _T('verifier:erreur_entier');
	
	// Pas de tableau ni d'objet
	if (is_numeric($valeur) and $valeur == intval($valeur)) {
		// Si c'est une chaine on convertit en entier
		$valeur = intval($valeur);
		$ok = true;
		$erreur = '';
		if (!isset($options['max'])) {
			$options['max'] = intval(_request('siege'));
		}
		if (!isset($options['min'])) {
			$options['min'] = 0;
		}
		
		if (isset($options['min'])) {
			$min_ok = ($ok and ($valeur >= $options['min']));
		}
		if (isset($options['max'])) {
			$max_ok = ($ok and ($valeur <= $options['max']));
		}

		if (!$max_ok and !$min_ok) {
			$erreur = _T('verifier:erreur_entier_entre', $options);
		} elseif (!$max_ok) {
			$erreur = _T('verifier:erreur_entier_max', array('max' => $options['max']));
		} elseif (!$min_ok) {
			$erreur = _T('verifier:erreur_entier_min',  array('min' => $options['min']));
		}
	}

	return $erreur;
}
