<?php
/**
 * Analyse de la recevalibité d'une liste électorale
 *
 * @plugin Listes électorales (prefix : op_elec)
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package SPIP\List_elec\Recevabilites
 */
 
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Fonction d'analyse de la recevabilité d'une liste électorale
 *
 * Cette fonction est appelée par le pipeline affiche_milieu lors de la vue de list_elec
 *
 * Variables significatives utilisées dans la fonction :
 * $id_list_scrutin : identifiant unique du scrutin
 * $id_list_elec : identifiant unique de la liste
 * $script : script de recevabilité de la liste 
 * $nb_total : nombre total des membres de la liste
 * $nb_femmes : nombre de femmes sur la liste
 * $nb_hommes : nombre d'hommes sur la liste
 *
 * @param  string    $fichier
 *  nécessaire. nom du fichier Yaml, sans l'extension,
 *  contenant les caractéristiques de l'analyse à faire.
 * @return string $sortie
 *  retourne un chaine vide ou formatée avec les informations de recevabilité.
**/
 
function recevabilites_analyse_de_recevabilite_dist($fichier=null,$id_list_elec=null,$id_list_scrutin=null){
	
	if (!$fichier or !$id_list_scrutin or !$id_list_elec) {
		return;
	}

	# Acquisition des membres de la liste triés par groupe puis par position
	if ($liens = sql_allfetsel('*', 'spip_list_membres', array(
			'id_list_elec=' . intval($id_list_elec),
			'statut NOT IN("refuse","poubelle")'
		),'','groupe, position')){ 

		if ($liens) {

			# Aquisition du script en Yaml @link:https://contrib.spip.net/Le-plugin-YAML-v2

			$fichier = find_in_path("recevabilites/${fichier}.yaml");
			include_spip('inc/yaml');
			$script = yaml_decode_file($fichier);
			
			# Rédaction du tableau d’analyse de recevabilité
			
			$tab = '<table class="spip liste" id="recevabilite">';

			# la tête du tableau (thead)

			$nb_total = count($liens);
			$tab .= '<thead><tr><th scope="col">';
			$tab .= 'Élément de recevabilité';
			$tab .= '</th><th scope="col">';
			$tab .= 'Constat';
			$tab .= '</th><th class="picto" scope="col">';
			$tab .= 'Recevabilité';
			$tab .= '</th></tr></thead>';
			
			# le pied du tableau (tfoot)

			$tab .= '<tfoot><tr>';
			$title = '';
			if (isset($script['listes']['global']['minimum']['explication'])) {
				$title .= _T(reforme_idiome($script['listes']['global']['minimum']['explication']));
			}
			if (isset($script['listes']['global']['imperatif']['explication'])) {
				$title .= _T(reforme_idiome($script['listes']['global']['imperatif']['explication']));
			}
			if ($title) {
				$tab .= '<th class="reponse_formulaire explication decale" title="' . $title . '">';
			} else {
				$tab .= '<th>';
			}
			$tab .= 'Total des membres de la liste';
			$tab .= '</th>';
			$tab .= '<th>';
			$nb_total = count($liens);
			$tab .= $nb_total;
			$tab .= '</th>';
			$title = ''; $class = 'success'; $donnee = $nb_total;

			// a-t-on un nombre global à atteindre, impératif ou minimum ?

			if (isset($script['listes']['global']['imperatif']['nb'])) {
				if ($script['listes']['global']['imperatif']['nb'] == $nb_total) {
					$title .= _T(reforme_idiome($script['listes']['global']['imperatif']['success']), array('nb'=> $nb_total));
					$class = 'success';
					$donnee = $nb_total;
				} else {
					$title .= _T(reforme_idiome($script['listes']['global']['imperatif']['error']), array('nb'=> $script['listes']['global']['imperatif']['nb']));
					$class = 'error';
					$donnee = ($nb_total - $script['listes']['global']['imperatif']['nb']);
				}
			} elseif (isset($script['listes']['global']['minimum']['nb'])) {
				foreach ($script['listes']['global'] as $cle => $valeurs) {
					if ($nb_total >= $script['listes']['global'][$cle]['nb']) {
						if ($nb_total == $script['listes']['global'][$cle]['nb'] or (($nb_total > $script['listes']['global'][$cle]['nb']) and ($cle != 'maximum'))) {
							$title .=  _T(reforme_idiome($script['listes']['global'][$cle]['success']), array('nb'=> $script['listes']['global'][$cle]['nb'])) . "\n";
						} elseif ($cle == 'maximum') {
							$title .=  _T(reforme_idiome($script['listes']['global'][$cle]['error']), array('nb'=> $script['listes']['global'][$cle]['nb'])) . "\n";
							$class = 'error';
						}
					} elseif (in_array($cle, array('minimum','maximum'))) {
						if ($cle == 'minimum') {
							$title .= _T(reforme_idiome($script['listes']['global'][$cle]['error']), array('nb'=> $script['listes']['global'][$cle]['nb']));
							$class = 'error';
						} else {
							$title .=  _T(reforme_idiome($script['listes']['global'][$cle]['reste']), array('nb'=> ($script['listes']['global'][$cle]['nb'] - $nb_total))) . "\n";
						}
					}
				}
			}

			# Spéficitées à prendre en compte globalement

			if (isset($script['listes']['specific'])) {
				if (isset($script['listes']['specific']['pair']['type']) and  $script['listes']['specific']['pair']['type'] == '2n') {
					if ($nb_total/2 != floor($nb_total/2)) {
						$title .=  _T(reforme_idiome($script['listes']['specific']['pair']['error']));
						$class = 'error';
					} else {
						$title .=  _T(reforme_idiome($script['listes']['specific']['pair']['success']));
					}
				}
			}
			$tab .= '<th class="reponse_formulaire ' . $class . ' center" title="' . $title . '">' . $donnee . '</th>';
			$tab .= '</tr></tfoot>';

			# le corps du tableau (tbody)

			$tab .= '<tbody>';

			# indication de la parité définie par un pourcentage

			if ($script['listes']['parite']['type']=='def'){
				// le nombre de femmes et d'hommes de la liste
				$nb_femmes = array_count_values(array_column($liens, 'genre'))['femme'];
				$nb_hommes = array_count_values(array_column($liens, 'genre'))['homme'];
				// la parité demandée pour la recevabilité de la liste
				$pc_femmes_effectif = $script['listes']['parite']['pc']['femmes'];
				$pc_hommes_effectif = $script['listes']['parite']['pc']['hommes'];
				// le minimum et le maximum numérique de la liste
				$nb_membres_minimum = $script['listes']['global']['minimum']['nb'];
				$nb_membres_maximum = $script['listes']['global']['maximum']['nb'];
				// analyse qualitative
				$tab .= '<tr>';
				if (isset($script['listes']['parite']['defaut']['explication'])){
					$tab .= '<td class="reponse_formulaire explication decale" title="' . _T(reforme_idiome($script['listes']['parite']['defaut']['explication'])) . '">';
				} else {
					$tab .= '<td>';
				}
				$tab .= _T('list_membre:champ_femme_label');
				$tab .= '</td><td>';
				$tab .= $nb_femmes;
				$tab .= '</td>';
				$title = ''; $class = 'success';
				$resultat = audit_parite_globale_dist ($nb_femmes,$nb_hommes,$pc_femmes_effectif,$pc_hommes_effectif,$nb_membres_minimum,$nb_membres_maximum);
				list($class, $title) = $resultat;
				$tab .= '<td class="reponse_formulaire ' . $class . ' valign center" title="' . $title . '"  rowspan="2">' . $nb_femmes . ' &#x2640; / &#x2642; ' . $nb_hommes . '</td></tr>'; 
				$tab .= '<tr><td>';
				$tab .=  _T('list_membre:champ_homme_label');
				$tab .= '</td><td>';
				$tab .= $nb_hommes;
				$tab .= '</td></tr>';
			}

			# indication de la parité définie globalement en alternance

			if (($script['listes']['parite']['type'] and $script['listes']['parite']['type'] == 'alterne')
				and
				($script['listes']['parite']['defaut']['mode'] and $script['listes']['parite']['defaut']['mode'] == 'global')){
				$nbg = $nbgf = $nbgh = $identifiant = 0;$precedent = $alterne = 'oui';$erreur_alternance='';

				foreach ($liens as $l => $valeurs) {
					$nbg++;
					if ($valeurs['genre'] == 'femme') {
						$nbgf++;
						if ($precedent == 'femme') {
							$alterne = 'non'; $erreur_alternance = $identifiant;
						}
						$precedent = 'femme'; $identifiant = $valeurs['id_list_membre'];
					} elseif ($valeurs['genre'] == 'homme') {
						$nbgh++;
						if ($precedent == 'homme') {
							$alterne = 'non'; $erreur_alternance = $identifiant;
						}
						$precedent = 'homme'; $identifiant = $valeurs['id_list_membre'];
					}
				}
				$tab .= '<td class="reponse_formulaire explication decale" title="' . _T(reforme_idiome($script['listes']['parite']['defaut']['explication'])) . '">' . _T(reforme_idiome($script['listes']['parite']['defaut']['titre'])) . '</td>';
				if ($alterne == 'oui') {
					$idiome = $script['listes']['parite']['defaut']['success'];
					$class = 'success';
					if ($idiome) {
						$inclure = _T(reforme_idiome($idiome));
					}
					$tab .= "<td>&#x2640;&nbsp;/&nbsp;&#x2642;</td>";
				} else {
					$idiome = $script['listes']['parite']['defaut']['error'];
					$class = 'error';
					if ($idiome) {
						$inclure = _T(reforme_idiome($idiome), array('id' => $erreur_alternance));
					}
					if ($precedent == 'homme') {
						$tab .= "<td>&#x2642;&nbsp;/&nbsp;&#x2642;</td>";
					} else {
						$tab .= "<td>&#x2640;&nbsp;/&nbsp;&#x2640;</td>";
					}
				}
				if (!$idiome) {  // on a pas l'idiome pour expliquer et on le dit
					spip_log(_T('list_elec:log_err_absence_idiome', array(
							'fichier' => $fichier,
							'chemin' => 'listes/parite/defaut/' . $class,
							'liste' => $id_list_elec,
							'scrutin' => $id_list_scrutin,
						)
					),
						'op_elec.' . _LOG_INFO_IMPORTANTE
					);
					$inclure = _T('list_elec:title_err_absence_idiome');
				}
				$tab .= '<td class="reponse_formulaire ' . $class . ' center" title="' . $inclure . '">' . $erreur_alternance . '</td></tr>';
			}

			$groupes_en_bdd = array_unique(array_column($liens, 'groupe')); // groupe existant
			$groupes = array();
			if ($script['groupes']) {
				foreach ($script['groupes'] as $cle => $valeur) {
					$groupes[] = $cle;
				}
			}
			$nb_groupes = count($groupes_en_bdd);

			# indications sur le nombre de groupes s'il s'agit d'une condition de recevabilité

			if (isset($script['listes']['sections']['imperatif']) and isset($script['listes']['sections']['imperatif']['nb']) and $script['listes']['sections']['imperatif']['nb']>0) {
				$tab .= '<tr>';
				if (isset($script['listes']['sections']['imperatif']) and isset($script['listes']['sections']['imperatif']['explication'])) {
					$tab .= '<td class="reponse_formulaire explication decale" title="' . _T(reforme_idiome($script['listes']['sections']['imperatif']['explication'])) . '">';
				} else {
					$tab .= '<td>';
				}
				$tab .= 'Nbre de groupes';
				$tab .= '</td><td>';
				$tab .= $nb_groupes;
				$tab .= '</td>';
				$nb_groupes_declares = count($groupes);
				if ($script['listes']['sections']['imperatif']['nb'] == $nb_groupes) {
					$tab .= '<td class="reponse_formulaire success center" title="' . _T(reforme_idiome($script['listes']['sections']['imperatif']['success']), array('nb'=> $nb_groupes)) . '">' . $nb_groupes . '</td></tr>';
				} else {
					$tab .= '<td class="error center" title="' . _T(reforme_idiome($script['listes']['sections']['imperatif']['error'])) . '">' . ($nb_groupes - $nb_groupes_declares) . '</td></tr>';
				}
			}

			# indications par groupes

			foreach ($groupes as $cle => $valeur) {
				$tab .= '<tr><td>';
				if (isset($script['groupes'][$valeur]['imperatif']['titre'])){
					$titre = _T(reforme_idiome($script['groupes'][$valeur]['imperatif']['titre']));
				} else {
					$titre = $valeur;
				}
				$tab .= $titre;
				$tab .= '</td>';
				$nbg = $nbgf = $nbgh = $identifiant = 0;$precedent = $alterne = '';
				foreach ($liens as $l => $valeurs) {
					if ($valeurs['groupe'] == $valeur) {
						$nbg++;
						if ($valeurs['genre'] == 'femme') {
							$nbgf++;
							if ($precedent == 'femme') {
								$alterne = 'non'; $erreur_alternance = $identifiant;
							}
							$precedent = 'femme'; $identifiant = $valeurs['id_list_membre'];
						} elseif ($valeurs['genre'] == 'homme') {
							$nbgh++;
							if ($precedent == 'homme') {
								$alterne = 'non'; $erreur_alternance = $identifiant;
							}
							$precedent = 'homme'; $identifiant = $valeurs['id_list_membre'];
						}
					}
				}
				$tab .= '<td>';
				$tab .= $nbg;
				$tab .= '</tb>';
				# nombre obligatoire par groupes
				// un nombre impératif

				if ($nb_imperatif = $script['groupes'][$valeur]['imperatif']['nb']){
					if ($nb_imperatif == $nbg){
						$idiome_precis = $script['groupes'][$valeur]['imperatif']['success'];
						$idiome_defaut = $script['listes']['sections']['imperatif']['defaut']['success'];
						$class = 'success';
						$nombre = $nbg;
						$idiome = $idiome_precis ? $idiome_precis : $idiome_defaut;
						if ($idiome) {
							$inclure = _T(reforme_idiome($idiome), array('nb'=> $nbg));
						}
					} else {
						$idiome_precis = $script['groupes'][$valeur]['imperatif']['error'];
						$idiome_defaut = $script['listes']['sections']['imperatif']['defaut']['error'];
						$class = 'error';
						$nombre = ($nbg - $script['groupes'][$valeur]['imperatif']['nb']);
						if ($nombre > 1) {
							$nombre = '+ ' . $nombre;
						}
						$idiome = $idiome_precis ? $idiome_precis : $idiome_defaut;
						if ($idiome) {
							$inclure = _T(reforme_idiome($idiome), array('nb'=> $nb_imperatif));
						}
					}
					if (!$idiome) {  // on a pas l'idiome pour expliquer et on le dit
						spip_log(_T('list_elec:log_err_absence_idiome', array(
								'fichier' => $fichier,
								'chemin' => 'listes/sections/imperatif/defaut/' . $class,
								'liste' => $id_list_elec,
								'scrutin' => $id_list_scrutin,
							)
						),
							'op_elec.' . _LOG_INFO_IMPORTANTE
						);
						$inclure = _T('list_elec:title_err_absence_idiome');
					}
					$tab .=  '<td class="reponse_formulaire ' . $class . ' center" title="' . $inclure . '">' . $nombre . '</td></tr>';
				} elseif (isset($script['groupes'][$valeur]['si'])) {
				// un nombre impératif... si un contexte particulier est rencontré
					if (isset($script['groupes'][$valeur]['si']['global'])) {
						if ($script['groupes'][$valeur]['si']['global'] == $nb_total) {
							if (isset($script['groupes'][$valeur]['si']['alors'])) {
								if ($script['groupes'][$valeur]['si']['alors'] == 'pair') {
									if (($nbg/2) == ceil($nbg/2)){
										$tab .= '<td class="reponse_formulaire success center" title="' . _T(reforme_idiome($script['groupes'][$valeur]['success'])) . '">' . $nbg . '</td></tr>';
									} else {
										$tab .= '<td class="reponse_formulaire error center" title="' . _T(reforme_idiome($script['groupes'][$valeur]['error'])) . '">' . $nbg . '</td></tr>';
									}
								}
							}
						} else {
							$tab .= '<td class="reponse_formulaire success center" title="' . _T(reforme_idiome($script['groupes'][$valeur]['inapplicable'])) . '">' . $nbg . '</td></tr>';
						}
					}
				} else {
					$tab .= '<td></td></tr>';
				}

				# si alternance ou appréciation globale, pas besoin de calculer par groupe

				if ((!$script['groupes'][$valeur]['alterne']['mode'] == 'groupe' and !$script['listes']['parite']['defaut']['mode']=='groupe')
					and 
					!$script['listes']['parite']['defaut']['mode'] == 'global') {
						
					$tab .= '<tr><td>';
					$tab .= 'Groupe '.$valeur.' Femmes';
					$tab .= '</td><td>';
					$tab .= $nbgf;
					$tab .= '</td></tr>';
	
					$tab .= '<tr><td>';
					$tab .= 'Groupe '.$valeur.' Hommes';
					$tab .= '</td><td>';
					$tab .= $nbgh;
					$tab .= '</td></tr>';
				}

				# calcul de la parité par alternance par groupe

				if (isset($script['groupes'][$valeur]['alterne']) or (isset($script['listes']['parite']['type']) and $script['listes']['parite']['type'] == 'alterne')) {
					$tab .= '<tr>';
					if ($script['groupes'][$valeur]['alterne']['explication']){
						$tab .= '<td class="reponse_formulaire explication decale" title="' . _T(reforme_idiome($script['groupes'][$valeur]['alterne']['explication'])) . '">';
					} elseif ($script['listes']['parite']['defaut']['explication']) {
						$tab .= '<td class="reponse_formulaire explication decale" title="' . _T(reforme_idiome($script['listes']['parite']['defaut']['explication'])) . '">';
					} else {
						$tab .= '<td>';
					}
					if ($script['groupes'][$valeur]['alterne']['titre']){
						$tab .= _T(reforme_idiome($script['groupes'][$valeur]['alterne']['titre']), array('groupe' => $valeur));
					} elseif ($script['listes']['parite']['defaut']['titre']){
						$tab .= _T(reforme_idiome($script['listes']['parite']['defaut']['titre']), array('groupe' => $valeur));
					} else {
						$tab .= $valeur;
					}
					$tab .= '</td><td>';
					$alterne = $alterne ? 'non' : 'oui';
					if ($nbg > 1){
						$tab .= $alterne;
					}
					$tab .= '</tb>';
					if ($script['groupes'][$valeur]['alterne']['mode'] == 'groupe' or $script['listes']['parite']['defaut']['mode'] == 'groupe'){
						// il est demandé une alternance dans le groupe
						if ($nbg > 1){
							if ($alterne == 'oui') {
								$idiome_precis = $script['groupes'][$valeur]['alterne']['success'];
								$idiome_defaut = $script['listes']['parite']['defaut']['success'];
								$class = 'success';
								$idiome = $idiome_precis ? $idiome_precis : $idiome_defaut;
								if ($idiome) {
									$inclure = _T(reforme_idiome($idiome));
								}
							} else {
								$idiome_precis = $script['groupes'][$valeur]['alterne']['error'];
								$idiome_defaut = $script['listes']['parite']['defaut']['error'];
								$class = 'error';
								$idiome = $idiome_precis ? $idiome_precis : $idiome_defaut;
								if ($idiome) {
									$inclure = _T(reforme_idiome($idiome), array('nb' => $erreur_alternance));
								}
							}
							if (!$idiome) {  // on a pas l'idiome pour expliquer et on le dit
								spip_log(_T('list_elec:log_err_absence_idiome', array(
										'fichier' => $fichier,
										'chemin' => 'listes/parite/defaut/' . $class,
										'liste' => $id_list_elec,
										'scrutin' => $id_list_scrutin,
									)
								),
									'op_elec.' . _LOG_INFO_IMPORTANTE
								);
								$inclure = _T('list_elec:title_err_absence_idiome');
							}
							$tab .= '<td class="reponse_formulaire ' . $class . ' center" title="' . $inclure . '">&#x2640; &#x2642;</td></tr>';
						}
					}
				} 
			} 

			$tab .= '</tbody></table>';
		}
	}

	$texte = $tab . '<br class="nettoyeur">';

	return $texte;
}

/**
 * Fonction d'analyse de la parité globale d'une liste électorale
 *
 * @param  int    $nb_femmes
 *                nombre de femmes de la liste
 * @param  int    $nb_hommes
 *                nombre de hommes de la liste
 * @param  int    $pc_femmes_effectif
 *                pourcentage de femmes exigé pour une liste recevable
 * @param  int    $pc_hommes_effectif
 *                pourcentage d'hommes exigé pour une liste recevable
 * @param  int    $nb_membres_minimum
 *                nombre minimum de candidats exigé pour une liste recevable
 * @param  int    $nb_membres_maximum
 *                nombre maximum de candidats exigé pour une liste recevable
 * @param  int    $debug
 *                informations supplémentaires permettant de comprendre les calculs
 * @return array  $class, $title
 *                valeurs de retour de la fonction indiquant le succès ou l'echec
 *                et la validation ou conseils de validation
**/
function audit_parite_globale_dist($nb_femmes,$nb_hommes,$pc_femmes_effectif,$pc_hommes_effectif,$nb_membres_minimum=0,$nb_membres_maximum=0,$debug='') {

	$nb_initial_femmes = $nb_femmes;
	$nb_initial_hommes = $nb_hommes;
	$title = '';
	$class = 'success';
	$end = "\n"; //"<br />"; 

	# vérifier les bornes maximales et reborner si nécessaire ; vérifier les bornes minimales

	if ($nb_membres_maximum) {
		$nb_hommes_maxi = ($nb_membres_maximum / 100) * $pc_hommes_effectif;
		$nb_femmes_maxi = ($nb_membres_maximum / 100) * $pc_femmes_effectif;
		if ($nb_femmes > $nb_femmes_maxi) {
			$retrait_borne_femmes = $nb_femmes - floor($nb_femmes_maxi);
			$nb_femmes = ceil($nb_femmes_maxi);
		}
		if ($nb_hommes > $nb_hommes_maxi) {
			$retrait_borne_hommes = $nb_hommes - ceil($nb_hommes_maxi);
			$nb_hommes = ceil($nb_hommes_maxi);
		}
		if ($nb_hommes + $nb_femmes > $nb_membres_maximum) {
			// il faudra retirer un homme ou une femme.
			$nb_hommes_necessaires = $nb_hommes_maxi;
			$nb_femmes_necessaires = $nb_femmes_maxi;
		}
	}
	if ($nb_membres_minimum) {
		$nb_hommes_mini = ($nb_membres_minimum / 100) * $pc_hommes_effectif;
		$nb_femmes_mini = ($nb_membres_minimum / 100) * $pc_femmes_effectif;
			if ($nb_femmes < floor($nb_femmes_mini)) {
				$title .= _T('list_recevabilite:texte_parite_global_pas_femmes_min',
					array ( 'nb' => $nb_femmes_mini ) ) . ' ';
			}
			if ($nb_hommes < floor($nb_hommes_mini)) {
				$title .= _T('list_recevabilite:texte_parite_global_pas_hommes_min',
					array ( 'nb' => $nb_hommes_mini ) ) . ' ';
			}
		if ($title) {
			return array ('error', $title);
		}
	}

	// nombre d'hommes nécessaires au regard du nombre de femmes :
	$nb_hommes_necessaires = $nb_femmes / $pc_femmes_effectif * $pc_hommes_effectif;
	// nombre de femmes nécessaires au regard du nombre d'hommes :
	$nb_femmes_necessaires = $nb_hommes / $pc_hommes_effectif * $pc_femmes_effectif;
	if ($debug) {
		$title .= "nb_femmes = " . ($nb_femmes + $retrait_borne_femmes) . $end;
		$title .= "nb_hommes = " . ($nb_hommes + $retrait_borne_hommes) . $end;
		$title .= "nb_membres_minimum = $nb_membres_minimum" . $end;
		$title .= "nb_membres_maximum = $nb_membres_maximum" . $end;
		$title .= "pc_femmes_effectif = $pc_femmes_effectif %" . $end;
		$title .= "pc_hommes_effectif = $pc_hommes_effectif %" . $end;
		$title .= _T('list_recevabilite:debug_nb_hommes_vs_femmes', array('nb' => $nb_hommes_necessaires)) . $end;
		$title .= _T('list_recevabilite:debug_nb_femmes_vs_hommes', array('nb' => $nb_femmes_necessaires)) . $end;
	}

	# on a suffisamment d'hommes au regard du nombre de femmes

	if (ceil($nb_hommes_necessaires) <= $nb_hommes) {
		if ($debug) {
			$title .= _T('list_recevabilite:debug_hommes_ok_femmes') . $end;
		}

	# une valeur avec une virgule (float)

		if (!ctype_digit(strval($nb_hommes_necessaires))){
			$arrondi_superieur_hommes = ceil($nb_hommes_necessaires);
			$arrondi_inferieur_hommes = floor($nb_hommes_necessaires);
			$nv_effectif_total = $arrondi_superieur_hommes + $nb_femmes;
			if ($nv_effectif_total < $nb_membres_minimum) {
				$title .= _T('list_recevabilite:texte_parite_global_pas_femmes_min');
				return array('error', $title);
			}
			$nv_effectif_pc_femmes = ($nv_effectif_total / 100) * $pc_femmes_effectif;
			$nv_effectif_pc_hommes = ($nv_effectif_total / 100) * $pc_hommes_effectif;
			if (($nb_initial_hommes > $arrondi_superieur_hommes) or ($nb_initial_femmes > $nb_femmes)) {
				$title .= _T('list_recevabilite:texte_parite_global_nb_candidatures') . ' ';
				$class = 'error';
			}
			if ($nb_initial_hommes - $arrondi_superieur_hommes == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_homme') . ' ';
			} elseif ($nb_initial_hommes - $arrondi_superieur_hommes > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_hommes', array('nb' => ($nb_initial_hommes - $arrondi_superieur_hommes))) . ' ';
			}
			if ($nb_initial_femmes - $nb_femmes == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femme') . ' ';
			} elseif ($nb_initial_femmes - $nb_femmes > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femmes', array('nb' => ($nb_initial_femmes - $nb_femmes))) . ' ';
			}
			$title .= _T('list_recevabilite:texte_parite_global_arrondi_inferieur', array (
				'total' => $nv_effectif_total,
				'femmes_pc_effectif' => $pc_femmes_effectif,
				'pc_femmes' => $nv_effectif_pc_femmes,
				'pc_hommes' => $nv_effectif_pc_hommes,
				'femmes' => $nb_femmes,
				'hommes' => $arrondi_superieur_hommes
				)
			);
			return array ($class, $title);
		} else {

		# un entier (int)

			$nv_effectif_total = $nb_hommes_necessaires + $nb_femmes;
			if ($nv_effectif_total < $nb_membres_minimum) {
				$title .= _T('list_recevabilite:texte_parite_global_pas_femmes_min');
				return array ('error', $title);
			}
			if (($nb_initial_hommes > $nb_hommes_necessaires) or ($nb_initial_femmes > $nb_femmes)) {
				$title .= _T('list_recevabilite:texte_parite_global_nb_candidatures') . ' ';
				$class = 'error';
			}
			if ($nb_initial_hommes - $nb_hommes_necessaires == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_homme') . ' ';
			} elseif ($nb_initial_hommes - $nb_hommes_necessaires > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_hommes', array('nb' => ($nb_initial_hommes - $nb_hommes_necessaires))) . ' ';
			}
			if ($nb_initial_femmes - $nb_femmes == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femme') . ' ';
			} elseif ($nb_initial_femmes - $nb_femmes > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femmes', array('nb' => ($nb_initial_femmes - $nb_femmes))) . ' ';
			}
			$title .= _T('list_recevabilite:texte_parite_global_entier', array(
				'total' => $nv_effectif_total,
				'femmes_pc_effectif' => $pc_femmes_effectif,
				'femmes' => $nb_femmes,
				'hommes' => $nb_hommes_necessaires
				)
			);
			return array ($class, $title);
		}
	}

	# on a suffisement de femmes au regard des hommes

	elseif (ceil($nb_femmes_necessaires) <= $nb_femmes) {
		if ($debug) {
			$title .= _T('list_recevabilite:debug_femmes_ok_hommes') . $end;
		}

	# une valeur avec une virgule (float)

		if (!ctype_digit(strval($nb_femmes_necessaires))){
			$arrondi_superieur_femmes = ceil($nb_femmes_necessaires);
			$arrondi_inferieur_femmes = floor($nb_femmes_necessaires);
			$nv_effectif_total = $arrondi_superieur_femmes + $nb_hommes;
			if ($nb_hommes_necessaires > $nb_hommes) {
				$title .= _T('list_recevabilite:texte_parite_global_pas_hommes_min');
				return array('error', $title);
			}
			$nv_effectif_pc_femmes = ($nv_effectif_total/100)*$pc_femmes_effectif;
			$nv_effectif_pc_hommes = ($nv_effectif_total/100)*$pc_hommes_effectif;
			if (($nb_initial_femmes > $arrondi_superieur_femmes) or ($nb_initial_hommes > $nb_hommes)){
				$title .= _T('list_recevabilite:texte_parite_global_nb_candidatures') . ' ';
				$class = 'error';
			}
			if ($nb_initial_femmes - $arrondi_superieur_femmes == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femme') . ' ';
			} elseif ($nb_initial_femmes - $arrondi_superieur_femmes > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femmes', array('nb' => ($nb_initial_femmes - $arrondi_superieur_femmes))) . ' ';
			}
			if ($nb_initial_hommes - $nb_hommes == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_homme') . ' ';
			} elseif ($nb_initial_hommes - $nb_hommes > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_hommes', array('nb' => ($nb_initial_hommes - $nb_hommes))) . ' ';
			}
			$title .= _T('list_recevabilite:texte_parite_global_arrondi_superieur', array(
				'total' => $nv_effectif_total,
				'femmes_pc_effectif' => $pc_femmes_effectif,
				'pc_femmes' => $nv_effectif_pc_femmes,
				'pc_hommes' => $nv_effectif_pc_hommes,
				'femmes' => $arrondi_superieur_femmes,
				'hommes' => $nb_hommes
				)
			);
			return array ($class, $title);

		# un entier (int)

		} else {
			$nv_effectif_total = $nb_femmes_necessaires + $nb_hommes;
			if ($nb_hommes_mini > $nb_hommes) {
				$title .= _T('list_recevabilite:texte_parite_global_pas_hommes_min', array('nb' => $nb_hommes_mini));
				return array('error', $title);
			}
			if (($nb_initial_hommes > $nb_hommes) or ($nb_initial_femmes > $nb_femmes_necessaires)) {
				$title .= _T('list_recevabilite:texte_parite_global_nb_candidatures') . ' ';
				$class = 'error';
			}
			if ($nb_initial_hommes - $nb_hommes == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_homme') . ' ';
			} elseif ($nb_initial_hommes - $nb_hommes > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_hommes', array('nb' => ($nb_initial_hommes - $nb_hommes))) . ' ';
			}
			if ($nb_initial_femmes - $nb_femmes_necessaires == 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femme') . ' ';
			} elseif ($nb_initial_femmes - $nb_femmes_necessaires > 1) {
				$title .= _T('list_recevabilite:texte_soustraire_femmes', array('nb' => ($nb_initial_femmes - $nb_femmes_necessaires))) . ' ';
			}
			$title .= _T('list_recevabilite:texte_parite_global_entier', array(
				'total' => $nv_effectif_total,
				'femmes_pc_effectif' => $pc_femmes_effectif,
				'femmes' => $nb_femmes_necessaires,
				'hommes' => $nb_hommes
				)
			);
			return array($class, $title);
		}
	}

}