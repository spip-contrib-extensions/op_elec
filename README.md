# op_elec

Gérer des opérations électorales, de la recevabilité aux résultats.

Pour une prise en main rapide, après installation du plugin, au sein de l'espace privé, selectionner le menu Edition | Opérations électorales et installer les exemples disponibles.

Pour une démo, sélectionner le menu Configuration | Gestion des plugins et cliquez sur Listes électorales | Démonstration.