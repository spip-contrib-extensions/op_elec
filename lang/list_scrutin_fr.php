<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

		
	'champ_acquisition_label' => 'Acquisition',
	'champ_acquisition_explication' => 'L’élection peut être acquise sans condition. Ou encore, l’élection est acquise au premier tour de scrutin si une liste recueille la majorité absolue des suffrages exprimés et, dans le cas contraire, il est procédé à un second tour.',
	'champ_acquisition_on_label' => 'sans condition',
	'champ_acquisition_501exp_label' => 'à la majorité absolue des suffrages exprimés',
	'champ_abrege_explication' => 'Limité à 25 caractères maximum. Utiliser des abréviations si nécessaire. La brièveté du titre est nécessaire pour l’affichage dans un espace réduit. ',
	'champ_abrege_label' => 'Titre court',
	'champ_abrege_placeholder' => 'Exemples : Scrutin du CM... | Région IDF - dept. 75',
	'champ_blancs_explication' => 'Cas où les bulletins blancs sont décomptés séparément des votes nuls et annexés au procès-verbal. Exemple : obligatoire en France depuis la loi du 21 février 2014',
	'champ_blancs_label' => 'Blancs',
	'champ_date_cloture_explication' => 'Indiquez la date et l’heure de cloture des scrutins',
	'champ_date_cloture_label' => 'Cloture du scrutin',
	'champ_date_depot_explication' => 'Indiquez la date butoire et l’heure pour déposer les listes',
	'champ_date_depot_label' => 'Dépot',
	'champ_date_ouverture_explication' => 'Indiquez la date et l’heure d’ouverture des scrutins',
	'champ_date_ouverture_label' => 'Ouverture du scrutin',
	'champ_exprimes_label' => 'Exprimés',
	'champ_id_list_scrutin_label' => 'Identifiant unique du scrutin',
	'champ_inscrits_explication' => 'Il s’agit du nombre d’inscrits sur les listes électorales. L’information est connue avant l’ouverture du scrutin.',
	'champ_inscrits_label' => 'Inscrits',
	'champ_lorsdelacloture_placeholder' => 'A renseigner après le dépouillement',
	'champ_nuls_label' => 'Nuls',
	'champ_nuls_explication' => 'Lors d’une élection, le vote nul consiste à mettre dans l’enveloppe une réponse qui n’est pas valable (bulletins déchirés, annotés...). La normalisation du vote électronique rend le vote nul inexistant.',
	'champ_pc_vote_label' => '%',
	'champ_prime_explication' => 'La prime majoritaire est une modalité du scrutin proportionnel plurinominal dans laquelle un nombre déterminé de sièges est attribué à la liste ayant obtenu le plus de voix. Les autres sièges sont distribués entre les listes gagnantes en fonction du nombre de voix obtenues. Elle est utilisée dans les élections municipales et régionales françaises. Laissez à zéro s’il n’y a pas de prime',
	'champ_prime_label' => 'dont prime majoritaire',
	'champ_pour_label' => ' pour ',
	'champ_quotient_electoral_label' => 'Quotien électoral',
	'champ_seuil_explication' => 'Seuil en pourcentage devant être dépassé pour que la liste soumise au scrutin ait des élu·es',
	'champ_seuil_label' => 'Seuil pour avoir des élu·es',
	'champ_recevabilite_explication' => 'Conditions de recevabilité de la liste (Ces conditions sont spécifiées dans un fichier Yaml du même nom placé dans le répertoire /recevabilites).',
	'champ_recevabilite_label' => 'Recevabilité',
	'champ_repartition_label' => 'Type de scrutin',
	'champ_repartition_moyenne_label' => 'Scrutin à la plus forte moyenne',
	'champ_repartition_explication' => 'Dans le cadre d’élections, les systèmes électoraux sont soit des scrutins utilisant la règle de la majorité, dits scrutins majoritaires, soit des systèmes cherchant à représenter plus ou moins fidèlement le vote des électeurs via le principe de la représentation proportionnelle, soit des systèmes mixtes alliant ces deux types de système.',
	'champ_repartition_reste_label' => 'Scrutin au plus fort reste',
	'champ_siege_explication' => 'Il s’agit du nombre de siège à pourvoir, connu avant l’ouverture du scrutin. Ce nombre sera répartit entre les listes, selon le résultat.',
	'champ_siege_label' => "Nombre de sièges",
	'champ_titre_explication' => 'Titre du scrutin, explicatif et complet. Inutile de mettre la date (Sera reprise comme date, celle de la clôture du scrutin).',
	'champ_titre_label' => 'Titre',
	'champ_titre_placeholder' => 'Exemples : Election municipale de la ville... | Election régionale d’Île-de-France, section départementale de Paris',
	'champ_votants_label' => 'Votants',
	'champ_seuil_elegibilite_explication' => 'Si le scrutin est un scrutin de listes à deux tour, pour qu’une liste puisse se présenter au second tour, elle doit avoir obtenu au premier tour un nombre de voix au moins égal à un certain seuil, en pourcentage, du nombre des suffrages exprimés.',
	'champ_seuil_elegibilite_label' => 'Seuil pour se présenter au second tour',
	'champ_seuil_inclusion_label' => 'Seuil pour bénéficier de l’inclusion de ses candidats',
	'champ_seuil_inclusion_explication' => 'Dans un scrutin de listes à deux tour, une liste peut ne pas pouvoir se présenter au second tour mais, compte tenu du dépassement d’un certain seuil, pouvoir faire l’objet d’une reprise de ses candidats par une liste éligible.',
	'erreur_calcul_sans_id' => 'Il est nécessaire d’avoir un scrutin identifié pour en calculer le résultat',
	'erreur_enregistrement_modification' => 'Pas de répartition enregistrée en modification (@err@)',
	'erreur_enregistrement_insertion' => 'Pas de répartition enregistrée en insertion',
	'fieldset_information' => 'Informations sur le scrutin',
	'fieldset_listes' => 'Résultats des listes',
	'fieldset_participation' => 'Participation',
	'icone_creer_list_scrutin' => 'Créer un scrutin',
	'icone_creer_resultat_scrutin' => 'Dépouillement & résultats',
	'icone_liaison' => 'Liaison d’un Objet au scrutin',
	'icone_modifier_list_scrutin' => 'Modifier ce scrutin',
	'icone_nouveau_list_scrutin' => 'Nouveau scrutin',
	'info_aucun_pc_inscrits' => '-',
	'info_aucun_pc_votants' => '-',
	'info_nb_pc_seuil' => '@nb@ % des voix',
	'info_nb_pc_inscrits' => '@nb@ % des inscrits',
	'info_nb_pc_votants' => '@nb@ % des votants',
	'info_nb_pc_blancs' => '@nb@ % de votes blancs',
	'info_1_list_scrutin' => '1 scrutin',
	'info_aucun_list_scrutin_operation' => 'Aucun autre scrutin de la même opération électorale',
	'info_nb_list_scrutins_operation' => '@nb@ autres scrutins de la même opération électorale',
	'info_1_list_scrutin_operation' => '1 autre scrutin de la même opération électorale',
	'info_1_siege_restant' => '1 siège restant à distribuer',
	'info_1_siege_quotient' => '1 siège attribué au quotient',
	'info_aucun_list_scrutin' => 'Aucun scrutin',
	'info_aucun_siege_restant' => 'Aucun siège restant à distribuer',
	'info_aucun_siege_quotient' => 'Aucun siège attribué au quotient',
	'info_nb_list_scrutins' => '@nb@ scrutins',
	'info_nb_votants' => '@nb@ votants',
	'info_nb_blancs' => '@nb@ blancs',
	'info_nb_sieges_restant' => '@nb@ sièges restant à distribuer',
	'info_nb_sieges_quotient' => '@nb@ sièges attribués au quotient',
	'info_numero_abbr' => 'N°',
	'info_objet_associe_1_list_scrutin' => 'L’objet est associé à 1 scrutin',
	'info_objet_associe_aucun_list_scrutin' => 'L’objet est associé à aucun scrutin',
	'info_objet_associe_nb_list_scrutins' => 'L’objet est associé à @nb@ scrutins',
	'modele_resultat_scrutin' => 'un résultat de scrutin',
	'modele_resultat_scrutin_graph' => 'un résultat graphique de scrutin',
	'log_calcul_des_resultats_out' => 'Calcul du scrutin n°@id@ par l’auteur n°@id_auteur@ dit @nom@ avec pour résultat : @erreur@',
	'log_enregistrement_des_resultats' => 'Enregistrement des résultats du scrutin n°@id@ par l’auteur n°@id_auteur@ dit @nom@ avec pour résultat : @modifs@',
	'log_erreur_enregistrement_modification' =>'Erreur dans l’enregistrement des résultats de la liste n°@id_liste@ du scrutin n°@id_scrutin@ par l’auteur n°@id_auteur@ dit @nom@ avec pour arguments : @modifs@',
	'log_info_forte_moyenne' => 'La plus forte moyenne est : @moyenne@',
	'lien_ajouter_list_scrutin' => 'Ajouter ce scrutin',
	'lien_retirer_list_scrutin' => 'Retirer ce scrutin',
	'lien_retirer_list_scrutins' => 'Retirer tous les scrutins',
	'saisie_titre' => 'Scrutins',
	'saisie_explication' => 'Permet de sélectionner un scrutin concernant une opération électorale...',
	'texte_equivalence' => 'Un tirage au sort sera nécessaire pour assurer l’attribution du siège car les listes sont équivalente tant en termes de voix que de nombre de candidats.',
	'texte_changer_statut' => 'Ce scrutin est :',
	'texte_nbre_liste' => 'Listes',
	'texte_renseigner_les_votes' => 'Renseigner les votes suite au dépouillement',
	'texte_siege_moyenne' => 'dont à répartir à la proportionnelle au quotient et à la plus forte moyenne',
	'texte_siege_quotient' => 'Siège(s) attribué(s) au quotient',
	'texte_siege_restant' => 'Siège(s) restant',
	'texte_siege_prime' => 'Siège(s) attribué(s) avec la prime majoritaire',
	'texte_total_siege_liste' => 'Total des sièges de la liste',
	'titre_langue_list_scrutin' => 'Langue de ce scrutin',
	'titre_list_scrutin' => 'Scrutin',
	'titre_list_scrutins' => 'Scrutins',
	'titre_logo_list_scrutin' => 'Logo du scrutin',
	'texte_acquisition_501exp' => 'Élection acquise à la majorité absolue',
	'texte_acquisition_on' => 'Élection acquise à l’issue du scrutin',
	'texte_acquisition_off' => 'Élection non acquise à l’issue du scrutin. Nouveau tour pour les listes éligibles.',
);

?>
