<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'list_elecs_nom' => "Listes électorales",
	'list_elecs_slogan' => "Faites vos listes !",
	'list_elecs_description' => "Créer des listes électorales pour diverses instances",
);

?>
