<?php
/**
 * @plugin Opérations électorales
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\lang
 */

/**
 * idiomes liés aux peuplements
 *
 * vous pouvez proposer des peuplements depuis vous prorpe plugin,
 * la création d'un fichier de lang identique à celui-ci vous permettra de personnaliser
 * les idiomes qui vous seraient nécessaires.
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

 # examen de la recevabilité : personnaliser davantage les informations données à l'utilisateur
 # idiomes pour l'exemple d'élection régionale

	'texte_groupe_alternance_error_d1' => 'La parité alternée pour le groupe du département 1 est défaillante ! Voir la candidate ou le candidat n°@nb@.',
	'texte_groupe_alternance_success_d1' => 'La parité alternée pour le groupe du département 1 est correcte...',
	'texte_imperatif_error_d1' => 'Vous n’avez pas le nombre candidats qu’il convient pour le département 1 ! Il faut atteindre le nombre @nb@ !',
	'texte_imperatif_success_d1' => 'Vous avez le nombre exact de candidats qu’il convient pour le département 1...',
	'titre_groupe_alternance_d1' => 'Parité alternée pour le groupe du département 1',
	'titre_groupe_d1' => 'Section départementale D1',
	'titre_groupe_d2' => 'Section départementale D2',
	'titre_groupe_d3' => 'Section départementale D3',
	'titre_groupe_d4' => 'Section départementale D4',
	'titre_groupe_d5' => 'Section départementale D5',
# Exemple - log : précisez pour le webmestre les informations de peuplement
	'log_elections_municipales_out' => 'action/list_elec_peuplement_elections_municipales. Auteur n°@id@ dit @nom@. @erreur@',
	'log_elections_regionales_out' => 'action/list_elec_peuplement_elections_regionales. Auteur n°@id@ dit @nom@. @erreur@',
		
# Exemple - titre et infobulle de l'encart sur le peuplement : être explicite et pédagogique pour celui qui peuplera
	'title_op_elec_peuplement_elections_regionales' => 'Les élections régionales d’une région fictive (rattachées pour l’exemple aux Hauts-de-France si vous avez installé le plugin Territoires). Une opération électorale de poids qui aurait eu lieu en 2015 : 1341 candidatures, 10 listes, 2 scrutins ! Vous pourrez observer que l’auteur n°1 a été indiqué (pour l’exemple) comme étant le candidat 1 de la liste 1.',
	'title_op_elec_peuplement_elections_municipales' => 'Les élections municipales d’une commune fictive membre d’une communauté d’agglomération tout aussi fictive (rattachées pour l’exemple à la commune d’Abbécourt si vous avez installé le plugin Territoires). Une opération électorale modeste qui aurait eu lieu en 2020 : 154 candidatures, 7 listes, 2 scrutins ! Vous pourrez observer que l’auteur n°1 a été indiqué (pour l’exemple) comme étant le candidat 1 de la liste 1.',
	'titre_op_elec_peuplement_elections_regionales' =>  'Un exemple d’élection régionale',
	'titre_op_elec_peuplement_elections_municipales' => 'Un exemple d’élection municipale',
		
# informations propre au plugin list_elec (transposition non souhaitable)
	'erreur_pas_autorisation' => 'Vous n’avez pas l’autorisation de procéder à cette action.',
	'erreur_pas_d_exemple' => 'Pas de peuplement possible de votre base de données avec des exemples pertinents',
	'erreur_titre_pas_autorisation' => 'Pas l’autorisation !',
	'erreur_titre_pas_d_exemple' => 'Pas d’exemples !',
	'titre_peuplement' => 'Peuplement',
	'log_insertion' => 'Insertion de @id_objet@ = @id@',
	'log_association' => 'Association : @assos@',
);

?>
