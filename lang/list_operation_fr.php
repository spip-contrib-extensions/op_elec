<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'champ_titre_explication' => 'Votre titre doit être complet. Il faut indiquer l’objet, le ressort la date (exemple : Elections régionales 2021 en Île-de-France)',
	'champ_id_list_operation_label' => 'Identifiant unique de l’opération électorale',
	'champ_date_debut_label' => 'Début des opérations',
	'champ_date_debut_explication' => 'Il ne s’agit pas d’une date juridique, mais du début de la mobilisation des équipes pour préparer le scrutin.',
	'champ_date_fin_label' => 'Fin des opérations',
	'champ_date_fin_explication' => 'Il ne s’agit pas d’une date juridique, mais de la date de la fin de l’opération électorale après le dépouillement et l’annonce des résultats. Mettez une date prévisionnelle pour bloquer votre calendrier et modifiez là si besoin.',
	'champ_date_fin_label' => 'Fin des opérations',
	'champ_titre_label' => 'Titre',
	'icone_creer_list_operation' => 'Créer une opération électorale',
	'icone_modifier_list_operation' => 'Modifier cette opération électorale',
	'icone_nouveau_list_operation' => 'Nouvelle opération électorale',
	'info_aucun_list_scrutindanslist_operation' => 'Aucun scrutin n’est lié à l’opération électorale',
	'info_1_list_operation' => '1 opération électorale',
	'info_aucun_list_operation' => 'Aucune opération électorale',
	'info_nb_list_operations' => '@nb@ opérations électorales',
	'info_numero_abbr' => 'N°',
	'info_objet_associe_1_list_operation' => '1 opération électorale liée à cet objet',
	'info_objet_associe_nb_list_operations' => '@nb@ opérations électorales liées à cet objet',
	'icone_liaison' => 'Liaison d’un Objet à l’opération électorale',
	'saisie_explication' => 'Permet de sélectionner une opération électorale',
	'saisie_titre' => 'Opérations électorales',
	'lien_ajouter_list_operation' => 'Ajouter cette opération électorale',
	'lien_retirer_list_operation' => 'Retirer cette opération électorale',
	'lien_retirer_list_operations' => 'Retirer toutes les opérations électorales',
	'texte_changer_statut' => 'Cette opération électorale est :',
	'titre_langue_list_operation' => 'Langue de cette opération électorale',
	'titre_list_operation' => 'Opération électorale',
	'titre_list_operations' => 'Opérations électorales',
	'titre_logo_list_operation' => 'Logo de l’opération électorale',
	'texte_nbre_liste' => 'Listes',
);

?>
