<?php
/**
 * Gestion du formulaire de configuration du plugin list_elec
 *
 * @plugin     Opérations électorales
 * prefix      op_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Description des saisies du formulaire de configuration
 *
 * @return array
 *     Description des saisies
 */
function formulaires_configurer_op_elec_saisies_dist(){
	$saisies = array(
		array( 
			'saisie' => 'choisir_objets',
			'options' => array(
				'nom' => 'objets_candidats',
				'label' => _T('list_elec:cfg_objets_candidats'),
				'explication' =>  _T('list_elec:cfg_objets_candidats_explication'),
				'exclus' => array('spip_list_elecs','spip_list_membres','spip_list_operations','spip_list_scrutins'),
			),
		),
		array( 
			'saisie' => 'choisir_objets',
			'options' => array(
				'nom' => 'objets_ressorts',
				'label' => _T('list_elec:cfg_objets_ressorts'),
				'explication' =>  _T('list_elec:cfg_objets_ressorts_explication'),
				'exclus' => array('spip_list_elecs','spip_list_membres','spip_list_operations','spip_list_scrutins','spip_auteurs'),
			),
		),
	);

	return $saisies;
	
}