<?php
/**
 * Gestion du formulaire d'édition d'un scrutin
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\op_elec\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

/*
 * Déclaration des champs du formulaire
 */
function formulaires_editer_list_scrutin_saisies_dist($id_list_scrutin='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_scrutin = intval($id_list_scrutin);

	// récupération d'id_list_operation dans l'URL si c'est une création
	if (!intval($id_list_scrutin) and intval(_request('id_list_operation'))){
		$operation = array( // numéro unique de l’opération liée au scrutin
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_operation', 
				'defaut' => _request('id_list_operation'),
			),
		);
	// si c'est une modification, on verifie si on a l'autorisation de modifier la parentalité 
	} elseif (intval($id_list_scrutin) and !autoriser('modifier', 'list_scrutin', $id_list_scrutin, '', array('parentalite' => 'oui'))){
		$operation = array( // numéro unique de l’opération liée au scrutin
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_operation', 
			),
		);
	} else {
		$operation = array( // numéro unique de l’opération liée au scrutin
			'saisie' => 'list_operations',
			'options' => array(
				'nom' => 'id_list_operation', 
				'label' => _T('list_operation:titre_list_operation'),
				'cacher_option_intro' => 'oui',
			),
		);
	}

	$saisies = array(
		array( // numéro unique du résultat
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_scrutin',
				'defaut' => $id_list_scrutin,
			),
		),$operation,
		array( // script d’examen de la recevabilité des listes soumises au scrutin
			'saisie' => 'input',
			'options' => array(
				'nom' => 'recevabilite',
				'label' => _T('list_scrutin:champ_recevabilite_label'),
				'explication' => _T('list_scrutin:champ_recevabilite_explication'),
			),
		),
		array( // son titre
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'label' => _T('list_scrutin:champ_titre_label'),
				'explication' => _T('list_scrutin:champ_titre_explication'),
				'placeholder' => _T('list_scrutin:champ_titre_placeholder'),
				'obligatoire' => 'oui'
			),
		),
		array( // son titre abrégé
			'saisie' => 'input',
			'options' => array(
				'nom' => 'abrege',
				'label' => _T('list_scrutin:champ_abrege_label'),
				'explication' => _T('list_scrutin:champ_abrege_explication'),
				'placeholder' => _T('list_scrutin:champ_abrege_placeholder'),
				'obligatoire' => 'oui'
			),
			'verifier' => array(
				'type' => 'taille',
				'options' => array(
					'min' => 1,
					'max' => 25
				)
			)
		),
		array( // les inscrits
			'saisie' => 'input',
			'options' => array(
				'nom' => 'inscrits',
				'label' => _T('list_scrutin:champ_inscrits_label'),
				'explication' => _T('list_scrutin:champ_inscrits_explication'),
				'obligatoire' => 'oui'
			),
		),
		array( // les sieges
			'saisie' => 'input',
			'options' => array(
				'nom' => 'siege',
				'label' => _T('list_scrutin:champ_siege_label'),
				'explication' => _T('list_scrutin:champ_siege_explication'),
				'obligatoire' => 'oui'
			),
		),
		array( // la prime
			'saisie' => 'input',
			'options' => array(
				'nom' => 'prime',
				'label' => _T('list_scrutin:champ_prime_label'),
				'explication' => _T('list_scrutin:champ_prime_explication'),
			),
		),
		array( // le seuil
			'saisie' => 'input',
			'options' => array(
				'nom' => 'seuil',
				'label' => _T('list_scrutin:champ_seuil_label'),
				'explication' => _T('list_scrutin:champ_seuil_explication'),
			),
		),
		array( // la repartition
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'repartition',
				'label' => _T('list_scrutin:champ_repartition_label'),
				'explication' => _T('list_scrutin:champ_repartition_explication'),
				'obligatoire' => 'oui',
				'data' => array(
					'moyenne' => _T('list_scrutin:champ_repartition_moyenne_label'),
					'reste' => _T('list_scrutin:champ_repartition_reste_label'),
				)
			),
		),
		array( // l'acquisition
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'acquisition',
				'label' => _T('list_scrutin:champ_acquisition_label'),
				'explication' => _T('list_scrutin:champ_acquisition_explication'),
				'obligatoire' => 'oui',
				'data' => array(
					'on' => _T('list_scrutin:champ_acquisition_on_label'),
					'501exp' => _T('list_scrutin:champ_acquisition_501exp_label'),
				)
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_depot',
				'label' => _T('list_scrutin:champ_date_depot_label'),
				'explication' => _T('list_scrutin:champ_date_depot_explication'),
				'horaire' => 'on',
				'heure_pas' => 5,
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
				'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_ouverture',
				'label' => _T('list_scrutin:champ_date_ouverture_label'),
				'explication' => _T('list_scrutin:champ_date_ouverture_explication'),
				'horaire' => 'on',
				'heure_pas' => 5,
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_cloture',
				'label' => _T('list_scrutin:champ_date_cloture_label'),
				'explication' => _T('list_scrutin:champ_date_cloture_explication'),
				'horaire' => 'on',
				'heure_pas' => 5,
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
	);
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet édité
 */
function formulaires_editer_list_scrutin_identifier_dist($id_list_scrutin='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_list_scrutin)));
}

/**
 * Déclarer les champs postés et y integrer les valeurs par défaut
 */
function formulaires_editer_list_scrutin_charger_dist($id_list_scrutin='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('list_scrutin',$id_list_scrutin,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	return $valeurs;
}

/**
 * Verifier les champs postés et signaler d'éventuelles erreurs
 */
function formulaires_editer_list_scrutin_verifier_dist($id_list_scrutin='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = array();
	$erreurs = formulaires_editer_objet_verifier('list_scrutin',$id_list_scrutin);
		
	return $erreurs;
}

/**
 * Traiter les champs postés
 */
function formulaires_editer_list_scrutin_traiter_dist($id_list_scrutin='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_scrutin = _request('id_list_scrutin');
	$retours = formulaires_editer_objet_traiter('list_scrutin',$id_list_scrutin,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	return $retours;
}