<?php
/**
 * Gestion du formulaire d'édition d'une opération électorale
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Op_elec\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

/*
 * Déclaration des champs du formulaire
 */
function formulaires_editer_list_operation_saisies_dist($id_list_operation='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_operation = intval($id_list_operation);
	
	$saisies = array(
		array( // numéro unique du résultat
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_operation',
				'defaut' => $id_list_operation,
			),
		),
		array( // son titre
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'label' => _T('list_operation:champ_titre_label'),
				'explication' => _T('list_operation:champ_titre_explication'),
					'obligatoire' => 'oui'
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_debut',
				'label' => _T('list_operation:champ_date_debut_label'),
				'explication' => _T('list_operation:champ_date_debut_explication'),
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_fin',
				'label' => _T('list_operation:champ_date_fin_label'),
				'explication' => _T('list_operation:champ_date_fin_explication'),
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
	);

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet édité
 */
function formulaires_editer_list_operation_identifier_dist($id_list_operation='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_list_operation)));
}

/**
 * Déclarer les champs postés et y integrer les valeurs par défaut
 */
function formulaires_editer_list_operation_charger_dist($id_list_operation='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('list_operation',$id_list_operation,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	unset($valeurs['id_list_operation']);
	return $valeurs;
}

/**
 * Verifier les champs postés et signaler d'éventuelles erreurs
 */
function formulaires_editer_list_operation_verifier_dist($id_list_operation='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = array();
	$erreurs = formulaires_editer_objet_verifier('list_operation',$id_list_operation);
		
	return $erreurs;
}

/**
 * Traiter les champs postés
 */
function formulaires_editer_list_operation_traiter_dist($id_abonnement='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_operation = _request('id_list_operation');

/*	// debug
	$valeurs = array(
		'retour' => _request('retour'),
		'id_list_operation' => _request('id_list_operation'),
		'titre' => _request('titre'),
		'nb_representants' => _request('nb_representants'),
		'pc_femmes' => _request('pc_femmes')
	);
	// $retours = array('message_erreur' => 'coucou'.print_r($valeurs,true));
*/

	$retours = formulaires_editer_objet_traiter('list_operation',$id_list_operation,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	return $retours;
}