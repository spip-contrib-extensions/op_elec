<?php
/**
 * Gestion du formulaire d'édition d'une liste électorale
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Op_elec\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

/*
 * Déclaration des champs du formulaire
 */
function formulaires_editer_list_elec_saisies_dist($id_list_elec='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_elec = intval($id_list_elec);

	// récupération d'id_list_scrutin dans l'URL si c'est une création
	if (!intval($id_list_elec) and intval(_request('id_list_scrutin'))){
		$scrutin = array( // numéro unique du scrutin lié à la liste
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_scrutin', 
				'defaut' => _request('id_list_scrutin'),
			),
		);
	// si c'est une modification, on verifie si on a l'autorisation de modifier la parentalité 
	} elseif (intval($id_list_elec) and !autoriser('modifier', 'list_elec', $id_list_elec, '', array('parentalite' => 'oui'))){
		$scrutin = array( // numéro unique de l’opération liée au scrutin
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_scrutin', 
			),
		);
	} else {
		$scrutin = array( // numéro unique de l’opération liée au scrutin
			'saisie' => 'list_scrutins',
			'options' => array(
				'nom' => 'id_list_scrutin', 
				'label' => _T('list_scrutin:titre_list_scrutin'),
				'explication' => _T('list_elec:champ_id_list_scrutin_explication'),
				'cacher_option_intro' => 'oui',
			),
		);
		}

	$saisies = array(
		array( // numéro unique de la liste électorale
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_elec',
				'defaut' => $id_list_elec,
			),
		),$scrutin,
		array( // son titre
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'label' => _T('list_elec:champ_titre_label'),
				'explication' => _T('list_elec:champ_titre_explication'),
					'obligatoire' => 'oui'
			),
		),
		array( // son titre abregé (25 caractères)
			'saisie' => 'input',
			'options' => array(
				'nom' => 'abrege',
				'label' => _T('list_elec:champ_abrege_label'),
				'explication' => _T('list_elec:champ_abrege_explication'),
				'maxlength' => 25, // un titre court
				'obligatoire' => 'oui'
			),
			'verifier' => array(
				'type' => 'taille',
				'options' => array(
					'min' => 1,
					'max' => 25
				)
			)
		),
		array( // les voix
			'saisie' => 'input',
			'options' => array(
				'nom' => 'voix',
				'label' => _T('list_elec:champ_voix_label'),
				'explication' => _T('list_elec:champ_voix_explication'),
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0
				)
			),
		),
	);
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet édité
 */
function formulaires_editer_list_elec_identifier_dist($id_list_elec='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_list_elec)));
}

/**
 * Déclarer les champs postés et y integrer les valeurs par défaut
 */
function formulaires_editer_list_elec_charger_dist($id_list_elec='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('list_elec',$id_list_elec,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	unset($valeurs['id_list_elec']);
	return $valeurs;
}

/**
 * Verifier les champs postés et signaler d'éventuelles erreurs
 */
function formulaires_editer_list_elec_verifier_dist($id_list_elec='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = array();
	$erreurs = formulaires_editer_objet_verifier('list_elec',$id_list_elec);
		
	return $erreurs;
}

/**
 * Traiter les champs postés
 */
function formulaires_editer_list_elec_traiter_dist($id_abonnement='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_elec = _request('id_list_elec');
	
	$retours = formulaires_editer_objet_traiter('list_elec',$id_list_elec,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	
	return $retours;
}