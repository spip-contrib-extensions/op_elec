<?php
/**
 * Gestion du formulaire d'édition pour le calcul du résultat d'un scrutin
 *
 * list_calcul n'est pas un objet,
 * c'est un formulaire permettant de changer simultanément les données des listes et du scrutin
 * pour prendre en compte le dépouillement. Il s'agit d'une facilité d'écriture.
 * Son objectif est de permettre de modifier en même temps plusieurs objets.
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Op_elec\Formulaires
 */


if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');


/*
 * Déclaration des champs du formulaire
 */
function formulaires_editer_list_calcul_saisies_dist($id_list_calcul='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_calcul = intval($id_list_calcul);
	$id_list_scrutin = intval(_request('id_list_scrutin'));

	# avoir les noms des listes et les voix
	$listes = array();
	$tous = sql_allfetsel('id_list_elec,titre,abrege,voix', 'spip_list_elecs', 'id_list_scrutin=' . $id_list_scrutin);
	// construire les saisies des voix qui se sont portées sur les listes
	foreach ($tous as $liste) {
		$listes[] = array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'id_list_elec-'.$liste['id_list_elec'],
				'label' => $liste['abrege'],
				'explication' => $liste['titre'],
				'defaut' => $liste['voix'],
				'obligatoire' => 'oui'
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0,
				),
			),
		);
	}

	# avoir les informations sur le scrutin
	$scrutin = sql_fetsel('blancs,nuls,inscrits,siege,prime,seuil,repartition,acquisition,seuil_elegibilite,seuil_inclusion', 'spip_list_scrutins', 'id_list_scrutin=' . $id_list_scrutin);
	
	$saisies = array(
		array( // le fieldset 
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'listes',
				'label' => _T('list_scrutin:fieldset_listes'),
				'icone' => 'identite-24.svg',
			),
			'saisies' =>  $listes,
			),
		array( // le fieldset 
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'participation',
				'label' => _T('list_scrutin:fieldset_participation'),
				'icone' => 'fiche-perso-24',
			),
			'saisies' => array( // les champs dans le fieldset 
				array( // les blancs
					'saisie' => 'input',
					'options' => array(
						'nom' => 'blancs',
						'label' => _T('list_scrutin:champ_blancs_label'),
						'explication' => _T('list_scrutin:champ_blancs_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['blancs']
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 0,
						),
					),
				),
				array( // les nuls
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nuls',
						'label' => _T('list_scrutin:champ_nuls_label'),
						'explication' => _T('list_scrutin:champ_nuls_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['nuls']
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 0,
						),
					),
				),
			),
		),
		array( // le fieldset 
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'information',
				'label' => _T('list_scrutin:fieldset_information'),
				'icone' => 'information-24',
			),
			'saisies' => array( // les champs dans le fieldset 
				array( // numéro unique du présent formulaire
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'id_list_calcul',
						'defaut' => $id_list_calcul,
					),
				),
					array( // numéro unique du scrutin
					'saisie' => 'hidden',
					'options' => array(
						'nom' => 'id_list_scrutin',
						'defaut' => $id_list_scrutin,
					),
				),
				array( // les inscrits
					'saisie' => 'input',
					'options' => array(
						'nom' => 'inscrits',
						'label' => _T('list_scrutin:champ_inscrits_label'),
						'explication' => _T('list_scrutin:champ_inscrits_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['inscrits']
					),
					'verifier' => array(
						'type' => 'list_inscrits'
					),
				),
				array( // les sieges
					'saisie' => 'input',
					'options' => array(
						'nom' => 'siege',
						'label' => _T('list_scrutin:champ_siege_label'),
						'explication' => _T('list_scrutin:champ_siege_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['siege']
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 1,
						),
					),
				),
				array( // la prime
					'saisie' => 'input',
					'options' => array(
						'nom' => 'prime',
						'label' => _T('list_scrutin:champ_prime_label'),
						'explication' => _T('list_scrutin:champ_prime_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['prime']
					),
					'verifier' => array(
						'type' => 'list_prime'
					),
				),
				array( // le seuil
					'saisie' => 'input',
					'options' => array(
						'nom' => 'seuil',
						'label' => _T('list_scrutin:champ_seuil_label'),
						'explication' => _T('list_scrutin:champ_seuil_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['seuil']
					),
					'verifier' => array(
						'type' => 'decimal',
						'options' => array(
							'min' =>'0.00',
							'max' => '99.99',
							'nb_decimales' => 2,
							'normaliser' => 'oui'
						)
					),
				),
				array( // la repartition
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'repartition',
						'label' => _T('list_scrutin:champ_repartition_label'),
						'explication' => _T('list_scrutin:champ_repartition_explication'),
						'obligatoire' => 'oui',
						'data' => array(
							'moyenne' => _T('list_scrutin:champ_repartition_moyenne_label'),
							'reste' => _T('list_scrutin:champ_repartition_reste_label'),
						),
						'defaut' => $scrutin['repartition']
					),
				),
				array( // l'acquisition
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'acquisition',
						'label' => _T('list_scrutin:champ_acquisition_label'),
						'explication' => _T('list_scrutin:champ_acquisition_explication'),
						'obligatoire' => 'oui',
						'data' => array(
							'on' => _T('list_scrutin:champ_acquisition_on_label'),
							'501exp' => _T('list_scrutin:champ_acquisition_501exp_label'),
						),
						'defaut' => $scrutin['acquisition']
					),
				),
				array( // le seuil_elegibilite
					'saisie' => 'input',
					'options' => array(
						'afficher_si' => '@acquisition@!="on"',
						'nom' => 'seuil_elegibilite',
						'label' => _T('list_scrutin:champ_seuil_elegibilite_label'),
						'explication' => _T('list_scrutin:champ_seuil_elegibilite_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['seuil_elegibilite']
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 0,
						),
					),
				),
				array( // le seuil_inclusion
					'saisie' => 'input',
					'options' => array(
						'nom' => 'seuil_inclusion',
						'afficher_si' => '@acquisition@!="on"',
						'label' => _T('list_scrutin:champ_seuil_inclusion_label'),
						'explication' => _T('list_scrutin:champ_seuil_inclusion_explication'),
						'obligatoire' => 'oui',
						'defaut' => $scrutin['seuil_inclusion']
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 0,
						),
					),
				),
			),
		),
	);
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet édité
 */
function formulaires_editer_list_calcul_identifier_dist($id_list_calcul='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_list_calcul)));
}

/**
 * Déclarer les champs postés et y integrer les valeurs par défaut
 */
function formulaires_editer_list_calcul_charger_dist($id_list_calcul='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){

	# empêcher l'appel du formulaire sans id_list_scrutin
	if (!intval(_request('id_list_scrutin'))){
		return array('message_erreur', T_('list_scrutin:erreur_calcul_sans_id'));
	}

	# charger les valeurs du formulaire sans passer par formulaires_editer_objet_charger() qui cherchera une table spip_list_calculs inexistante
	$chargement = charger_fonction('editer_list_calcul_saisies','formulaires');
	$valeurs = $chargement();

	# sécurité sur l'autorisation détenue par l'auteur pour procéder au dépouillement et calcul
	include_spip('inc/autoriser');
	if (!autoriser('creer','list_calcul')){
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Verifier les champs postés et signaler d'éventuelles erreurs
 */
function formulaires_editer_list_calcul_verifier_dist($id_list_calcul='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = array();
	$erreurs = formulaires_editer_objet_verifier('list_calcul',$id_list_calcul);

	return $erreurs;
}

/**
 * Traiter les champs postés
 */
function formulaires_editer_list_calcul_traiter_dist($id_list_calcul='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$id_list_scrutin = _request('id_list_scrutin');
	$modifs = array();

	# enregistrement des valeurs modifiées du scrutin dans l'objet éditorial
	include_spip('action/editer_objet');
	$scrutin = array(
		'blancs' => _request('blancs'),
		'nuls' => _request('nuls'),
		'inscrits' => _request('inscrits'),
		'siege' => _request('siege'),
		'prime' => _request('prime'),
		'seuil' => _request('seuil'),
		'repartition' => _request('repartition'),
		'acquisition' => _request('acquisition'),
		'seuil_elegibilite' => _request('seuil_elegibilite'),
		'seuil_inclusion' => _request('seuil_inclusion'),
	);
	$erreur = objet_modifier('list_scrutin', $id_list_scrutin, $scrutin);
	if ($erreur) {
		return array ('message_erreur' => $erreur);
	} else {
		$modifs[] = array('id_list_scrutin' => $id_list_scrutin, 'data' => $scrutin);
	}
	
	# enregistrement des valeurs modifiées des listes présentées au scrutin dans les objets éditoriaux
	$tous = sql_allfetsel('id_list_elec,abrege,voix', 'spip_list_elecs', 'id_list_scrutin=' . $id_list_scrutin);
	foreach ($tous as $liste) {
		$voix = _request('id_list_elec-'.$liste['id_list_elec']);
		$erreur = objet_modifier('list_elec', $liste['id_list_elec'], array('voix' => $voix));
		if ($erreur) {
			return array ('message_erreur' => $erreur);
		} else {
			$modifs[] = array(
				'id_list_elec' => $liste['id_list_elec'],
				'data' => array('voix' => $voix)
			);
		}
	}
	
	# indiquer dans le log l'opération d'enregistrement des différents modifications
	spip_log(_T('list_scrutin:log_enregistrement_des_resultats', array(
			'id' => $id_list_scrutin,
			'id_auteur' => $GLOBALS['auteur_session']['id_auteur'],
			'nom' => $GLOBALS['auteur_session']['nom'],
			'modifs' => print_r($modifs,TRUE)
		)
	),
	'op_elec.' . _LOG_INFO_IMPORTANTE
	);

	$scrutin = sql_fetsel('siege,prime,seuil,repartition,acquisition,seuil_elegibilite,seuil_inclusion', 'spip_list_scrutins', 'id_list_scrutin=' . intval($id_list_scrutin));

	$toutes = sql_allfetsel('id_list_elec,voix', 'spip_list_elecs', 'id_list_scrutin=' . intval($id_list_scrutin));

	include_spip('inc/op_elec');
	
	# chargement et appel de la fonction de calcul des résultats
	$calcul_des_resultats = charger_fonction('calcul_des_resultats','op_elec');
	$repartition = $calcul_des_resultats($scrutin,$toutes);

	# enregistrement des sièges obtenus dans la table auxiliaire
	foreach ($repartition as $id_list_elec => $tableau) {
		$champs = array(
			'id_list_scrutin' => $id_list_scrutin,
			'id_list_elec' => $id_list_elec,
			'pourcentage_des_suffrages' => $tableau['pourcentage_des_suffrages'],
			'repartition_au_quotient' => $tableau['repartition_au_quotient'],
			'prime' => $tableau['prime'],
			'attribution_moyenne' => $tableau['attribution_moyenne'],
			'class' => $tableau['class'],
			'specificites' => $tableau['specificites']
		);
		# modification
		if ($id_list_repartition = sql_getfetsel('id_list_repartition', "spip_list_repartitions", array ("id_list_elec=$id_list_elec", "id_list_scrutin=$id_list_scrutin"))){
			$champs = array_merge($champs, array ('id_list_repartition' => $id_list_repartition));
			if ($retour = objet_modifier('list_repartition', intval($id_list_repartition), $champs)){
				spip_log(_T('list_scrutin:log_erreur_enregistrement_modification', array(
						'id_liste' => $id_list_elec,
						'id_scrutin' => $id_list_scrutin,
						'id_auteur' => $GLOBALS['auteur_session']['id_auteur'],
						'nom' => $GLOBALS['auteur_session']['nom'],
						'modifs' => print_r($champs,TRUE)
					)
				),
				'op_elec.' . _LOG_ERREUR
				);
				return array('message_erreur' => _T('list_scrutin:erreur_enregistrement_modification', array(
					'err' => $retour)));
			}
		} else {
		# insertion
			if (!$id_list_repartition = objet_inserer('list_repartition', '', $champs)){
				return array('message_erreur' => _T('list_scrutin:erreur_enregistrement_insertion'));
			}
		
		}
	}

	# rediriger vers la page de contenu et non pas d'édition (par défaut la page boucle sur elle-même.)
	return array(
		'message_ok' => 'Ok', // symbolique
		'redirect' => generer_url_ecrire('list_scrutin', 'id_list_scrutin=' . $id_list_scrutin),
	);
}
