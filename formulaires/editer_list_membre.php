<?php
/**
 * Gestion du formulaire d'édition d'un membre d'une liste électorale
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Op_elec\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

/*
 * Déclaration des champs du formulaire
 *
 * @use objet_type
 *   table_objet() retrouve l'objet à partir de la table ; objet_type() est plus polyvalent
 *   et retrouve l'objet à partir du nom d'objet ou de table
 */

function formulaires_editer_list_membre_saisies_dist($id_list_membre='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){

	$id_list_membre = intval($id_list_membre);

	$saisies = array();

	# connaitre la liste du membre 
	if (!$id_list_elec = intval(_request('id_list_elec'))) {
		$id_list_elec = sql_getfetsel('id_list_elec', 'spip_list_membres', 'id_list_membre=' . intval($id_list_membre));
	}

	# avoir le script pour déterminer les groupes et candidatures supplémentaires possibles
	if (intval($id_list_elec)) {
		$fichier = sql_getfetsel('scrutin.recevabilite', 'spip_list_elecs AS liste LEFT JOIN spip_list_scrutins AS scrutin ON liste.id_list_scrutin=scrutin.id_list_scrutin', 'id_list_elec=' . intval($id_list_elec));
		if ($fichier) {
			# Aquisition du script en Yaml @link:https://contrib.spip.net/Le-plugin-YAML-v2
			$fichier = find_in_path("recevabilites/${fichier}.yaml");
			include_spip('inc/yaml');
			$script = yaml_decode_file($fichier);
			if ($script) {
			}
			// liste des groupes possibles dans la même liste (exemple : arrondissements pour les municipales, sections départementales pour les régionales)
			$data_groupes = array();
			if ($script['groupes']){
				foreach ($script['groupes'] as $cle => $valeurs) {
					$label='';
					if ($valeurs['imperatif']['titre']) {
						$label = _T(reforme_idiome($valeurs['imperatif']['titre']),array(),array('force'=>''));
					}
					$label = $label ? $label : $cle;
					$data_groupes = array_merge($data_groupes, array($cle => $label));
				}				
			}
			if ($data_groupes) {
				$groupe = array( // son groupe
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'groupe',
						'label' => _T('list_membre:champ_groupe_label'),
						'explication' => _T('list_membre:champ_groupe_explication'),
						'data' => $data_groupes,
						'obligatoire' => 'oui',
					)
				);
			}
			// liste des mandats possible dans la même liste (exemple : être candidat pour conseiller municipal et  conseiller communautaire)
			$data_mandats = array();
			if ($script['mandats']) {
				foreach ($script['mandats'] as $cle => $valeurs) {
					$label='';
					if ($valeurs['titre']) {
						$label = _T(reforme_idiome($valeurs['titre']),array(),array('force'=>''));
					}
					$label = $label ? $label : $cle;
					$data_mandats = array_merge($data_mandats, array($cle => $label));
				}
			}
			if ($data_mandats and count($data_mandats) > 1 and count($data_mandats) < 3 ) {
				// Supprimer le premier élément du tableau qui est le mandat par défaut
				$supprime = array_shift($data_mandats);
				foreach ($data_mandats as $cle => $valeur) {
					$label_case = $valeur;
					$valeur_oui = $cle;
				}
				
				$suppl = array( // son groupe
					'saisie' => 'case',
					'options' => array(
						'nom' => 'suppl',
						'label' => _T('list_membre:champ_suppl_label'),
						'explication' => _T('list_membre:champ_suppl_explication'),
						'label_case' => $label_case,
						'valeur_oui' => $valeur_oui,
					)
				);
			}
			// to do : plus de deux mandats.
		} 
	} 
	if (!isset($groupe)) {
		$groupe = array( // son groupe
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'groupe',
			)
		);
	}
	if (!isset($suppl)) {
		$suppl = array( // les candidatures supplémentaires
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'suppl',
			),
		);
	}

	if (!$id_list_membre){ // cas d'une création avec un possible flechage de l’id_list_elec
		$id_list_elec = intval(_request('id_list_elec'));
		$position = intval(_request('position'));
		if (!$position) {
			// determiner rang judicieux.
			if ($id_list_elec){
				$position = sql_getfetsel('Max(position)', 'spip_list_membres', array(
						'id_list_elec=' . intval($id_list_elec),
						// to do : ajouter une prise en considération par sous ensemble (groupe)
						"statut != 'poubelle'"
					)
				);
			}
			if (!$position) {
				$position = 1;
			} else {
				$position++;
			}
		}
	}

	# permettre que l'utilisateur modifie la liste défini, sauf si s’il n'en pas a l'autorisation.
	if ($id_list_membre and !autoriser('modifier', 'list_membre', $id_list_membre, '', array('parentalite' => 'oui'))){ 
		$list_elec = array( // numéro unique de la liste électorale
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_list_elec',
				'defaut' => $id_list_elec
			),
		);
	} else {
		$list_elec = array( // liste des listes electorales
			'saisie' => 'list_elecs',
			'options' => array(
				'nom' => 'id_list_elec',
				'label' => _T('list_elec:titre_list_elec'),
				'obligatoire' => 'oui',
				'defaut' => $id_list_elec
			),
		);
	}

	
	$saisies[] = array(
		'saisie' => 'fieldset',
		'options' => array(
			'nom' => 'fieldset_position_dans_la_liste',
			'label' => _T('list_membre:fieldset_position_dans_la_liste'),
		),
		'saisies' => array(
			array( // numéro unique du membre
				'saisie' => 'hidden',
				'options' => array(
					'nom' => 'id_list_membre',
					'defaut' => $id_list_membre
				)
			),
			$list_elec,
			array( // le rang
				'saisie' => 'input',
				'options' => array(
					'nom' => 'position',
					'label' => _T('list_membre:champ_position_label'),
					'explication' => _T('list_membre:champ_position_explication'),
					'type' => 'number',
					'min' => 1,
					'defaut' => $position
				),
				'verifier' => array(
					'type' => 'entier',
					'options' => array('min' => 1),
				),
			),
				$groupe,$suppl,
		)
	);

	
	$saisies[] = array(
		'saisie' => 'fieldset',
		'options' => array(
			'nom' => 'fieldset_caracteristiques',
			'label' => _T('list_membre:fieldset_caracteristiques'),
		),
		'saisies' => array(
			array( // son nom
				'saisie' => 'input',
				'options' => array(
					'nom' => 'nom',
					'label' => _T('list_membre:champ_nom_label'),
					'obligatoire' => 'oui'
				),
			),
			array( // son genre
				'saisie' => 'radio',
				'options' => array(
					'nom' => 'genre',
					'label' => _T('list_membre:champ_genre_label'),
					'data' => array('femme' => _T('list_membre:champ_femme_label'), 'homme' => _T('list_membre:champ_homme_label')),
					'obligatoire' => 'oui'
				),
			)
		)
	);

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet édité
 */

function formulaires_editer_list_membre_identifier_dist($id_list_membre='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_list_membre)));
}

/**
 * Déclarer les champs postés et y integrer les valeurs par défaut
 */

function formulaires_editer_list_membre_charger_dist($id_list_membre='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('list_membre',$id_list_membre,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	unset($valeurs['id_list_membre']);
	return $valeurs;
}

/**
 * Verifier les champs postés et signaler d'éventuelles erreurs
 */

function formulaires_editer_list_membre_verifier_dist($id_list_membre='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = array();
	$erreurs = formulaires_editer_objet_verifier('list_membre',$id_list_membre);
		
	return $erreurs;
}

/**
 * Traiter les champs postés
 */

function formulaires_editer_list_membre_traiter_dist($id_list_membre='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$retours = array();
	set_request('id_auteur',''); // nécessaire pour que l'association à l'auteur courant ne soit pas faite

	$retours = formulaires_editer_objet_traiter('list_membre',$id_list_membre,'',$lier_trad,$retour,$config_fonc,$row,$hidden);

	return $retours;
}