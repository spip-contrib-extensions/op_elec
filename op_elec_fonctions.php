<?php
/**
 * Fonctions utiles au plugin Liste électorales (prefix:List_elec)
 *
 * @plugin     Opérations électorales
 * prefix      op_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\
 *
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * FILTRE pour permettre l'affichage d'un picto de l'objet
 * associé au membre de la liste.
 *
 * Il est possible de surcharger ce filtre pour personnaliser davantage le picto.
 * Pour ce faire créer une fonction filtre_picto_assos() dans votre plugin
 * au niveau du fichier mon_plugin_fonctions.php.
 *
 * @exemple: [(#ID_LIST_MEMBRE|picto_assos{list_membre})]
 * @param string $id
 *     text transmis au filtre. Numéro d'identifiant unique
 * @param string $objet
 *     text transmis au filtre. Objet concerné par l'identifiant unique
 * @return string
 *     retour du filtre. Image formatée en HTML de l'objet associé à l'objet transmis ou chaine vide.
 */

function filtre_picto_assos_dist($id_objet_associable,$objet_associable) {
	include_spip('inc/config'); // pour la fonction lire_config()
	include_spip('inc/utils'); // pour la fonction generer_url_ecrire()
	include_spip('action/editer_liens'); // pour la fonction objet_trouver_liens()

	$picto = '';
	if (intval($id_objet_associable) and in_array($objet_associable,array('list_elec','list_membre','list_operation'))){
		if ($objet_associable == 'list_membre') {
			$objets_candidats = lire_config('op_elec/objets_candidats');
			$tables = lister_tables_spip();
			$tables = array_flip($tables);
			// des objets associables...
			if ($objets_candidats) {
				foreach ($objets_candidats as $table){
					// ... avec une table d'association ...
					if ($table and in_array($table . "_liens",$tables)) {
						// ... dont on a l'objet qui correspond à celui sollicité...
						$objet = objet_type($table);
						// ... et le picto ...
						$img = objet_icone($objet,16);
						// .. et pour lequel il faudra vérifier s'il y a une association au id_list_membre transmis.
						$liens = objet_trouver_liens(array($objet => '*'), array('list_membre' => $id_objet_associable));
						if ($liens){
							foreach ($liens as $lien) {
							$url = generer_url_entite($lien['id_' . $objet],$objet);
							$picto .= "<a href='$url'>" . $img . "</a>"; 
							}
						}
					}
				}
			}
		}
		if (in_array($objet_associable, array('list_elec','list_operation'))) {
			$objets_ressorts = lire_config('op_elec/objets_ressorts');
			$tables = lister_tables_spip();
			$tables = array_flip($tables);
			// des objets associables...
			if ($objets_ressorts) {
				foreach ($objets_ressorts as $table){
					// ... avec une table d'association ...
					if ($table and in_array($table . "_liens",$tables)) {
						// ... dont on a l'objet...
						$objet = objet_type($table);
						// ... et le picto ...
						$img = objet_icone($objet,16);
						// .. et pour lequel il faudra vérifier s'il y a une association au id_list_elec ou operation transmis.
						$liens = objet_trouver_liens(array($objet => '*'), array($objet_associable => $id_objet_associable));
						if ($liens){
							foreach ($liens as $lien) {
							$url = generer_url_ecrire($objet, "id_" . $objet . "=" . $lien['id_' . $objet]);
							$picto .= "<a href='$url'>" . $img . "</a>"; 
							}
						}
					}
				}
			}
		}
	}
	
    return $picto;
}

/**
  * reformer la chaine de langue à traduite.
  *
  * @param string $chaine
  * @return string
  */
 function reforme_idiome($chaine) {
 	$chaine = trim($chaine);
 	if (preg_match("/^(&lt;:|<:)/", $chaine)) {
 		$chaine = preg_replace("/^(&lt;:|<:)/", "", $chaine);
 		$chaine = preg_replace("/(:&gt;|:>)$/", "", $chaine);
 		return $chaine;
 	}
 
 	return $chaine;
}
