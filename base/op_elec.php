<?php

/**
 * @plugin Listes électorales (prefix : list_elec)
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\List_elec\Base
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Déclaration des objets éditoriaux
 *
 * @param array $tables
 * @return array
 */

function op_elec_declarer_tables_objets_sql($tables){

	$tables['spip_list_membres'] = array(
		'texte_retour' => 'icone_retour',
		'texte_objets' => 'list_membre:titre_list_membres',
		'texte_objet' => 'list_membre:titre_list_membre',
		'texte_modifier' => 'list_membre:icone_modifier_list_membre',
		'texte_creer' => 'list_membre:icone_nouveau_list_membre',
		'info_aucun_objet'=> 'list_membre:info_aucun_list_membre',
		'info_1_objet' => 'list_membre:info_1_list_membre',
		'info_nb_objets' => 'list_membre:info_nb_list_membres',
		'texte_logo_objet' => 'list_membre:titre_logo_list_membre',
		'texte_langue_objet' => 'list_membre:titre_langue_list_membre',
		'type' => 'list_membre',
		'principale' => 'oui',
		'table_objet_surnoms' => array('listmembre'),
		'field'=> array(
			'id_list_membre' => "bigint(21) NOT NULL",
			'id_list_elec' => "bigint(21) NOT NULL",
			'nom'  => "varchar(255) DEFAULT '' NOT NULL",
			'genre' => "enum ('femme','homme')",
			'position' => "bigint(21) NOT NULL",
			'groupe' => "varchar(255) DEFAULT '' NOT NULL",
			'suppl' => "varchar(255) DEFAULT '' NOT NULL",
			'date' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'statut' => "varchar(255) DEFAULT '' NOT NULL",
			'lang' => "varchar(10) DEFAULT '' NOT NULL",
			'langue_choisie' => "varchar(3) DEFAULT 'non'",
			'id_trad' => "bigint(21) DEFAULT '0' NOT NULL",
			'maj' => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY" => "id_list_membre",
			"KEY" => "id_list_elec",
			"KEY" => "groupe",
			"KEY" => "position",
		),
		'titre' => "nom AS titre, '' AS lang",
		'date' => "date",
		'champs_editables' => array(
			 "id_list_elec", "nom", "genre", "groupe", "position", "suppl"
		),
		'champs_versionnes' => array(
			 "id_list_elec", "nom", "genre", "groupe", "position", "suppl"
		),
		'rechercher_champs' => array(
			'nom' => 10, 'groupe' => 8
		),
		'tables_jointures' => array(),
		'statut'=> array(
			array(
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'statut_textes_instituer' => array(
			'prepa' => 'texte_statut_en_cours_redaction',
			'prop' => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			'refuse' => 'texte_statut_refuse',
			'remplace' => 'list_membre:texte_remplace_statut',
			'poubelle' => 'texte_statut_poubelle',
		),
		// images du statut supplémentaire 'remplacé' (images placée dans /images)
		// mais l'ajout nécessite une redéclaration de l'ensemble.
		'statut_images' => array(
			'prepa'    => 'puce-preparer-8.png',
			'prop'     => 'puce-proposer-8.png',
			'publie'   => 'puce-publier-8.png',
			'refuse'   => 'puce-refuser-8.png',
			'remplace' => 'puce-remplace-8.png',
			'poubelle' => 'puce-supprimer-8.png',
		),
		'texte_changer_statut' => 'list_membre:texte_changer_statut',
	);
	
	$tables['spip_list_elecs'] = array(
		'texte_retour' => 'icone_retour',
		'texte_objets' => 'list_elec:titre_list_elecs',
		'texte_objet' => 'list_elec:titre_list_elec',
		'texte_modifier' => 'list_elec:icone_modifier_list_elec',
		'texte_creer' => 'list_elec:icone_nouvelle_list_elec',
		'info_aucun_objet'=> 'list_elec:info_aucun_list_elec',
		'info_1_objet' => 'list_elec:info_1_list_elec',
		'info_nb_objets' => 'list_elec:info_nb_list_elecs',
		'texte_logo_objet' => 'list_elec:titre_logo_list_elec',
		'texte_langue_objet' => 'list_elec:titre_langue_list_elec',
		'type' => 'list_elec',
		'principale' => "oui",
		'table_objet_surnoms' => array('listelec'),
		'field'=> array(
			"id_list_elec" => "bigint(21) NOT NULL",
			"id_list_scrutin" =>  "bigint(21) NOT NULL",
			"titre"  => "varchar(255) DEFAULT '' NOT NULL",
			"abrege"  => "varchar(25) DEFAULT '' NOT NULL",
			# 1 colonne pour les résultats
			'voix' => 'bigint(21) NOT NULL',
			"date" => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			"statut" => "varchar(255) DEFAULT '' NOT NULL",
			"lang" => "varchar(10) DEFAULT '' NOT NULL",
			"langue_choisie" => "VARCHAR(3) DEFAULT 'non'",
			"id_trad" => "bigint(21) DEFAULT '0' NOT NULL",
			"maj" => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY" => "id_list_elec",
			"KEY" => "id_list_scrutin",
		),
		'titre' => "titre AS titre, '' AS lang",
		'date' => "date",

		'champs_editables' => array(
			'id_list_scrutin', 'titre', 'abrege', 'voix'
		),
		'champs_versionnes' => array(
			'id_list_scrutin', 'titre', 'abrege', 'voix'
		 ),
		'rechercher_champs' => array(
			'titre' => 8, 'abrege' => 10
		),
		'tables_jointures' => array(
			'list_elecs_liens',
			'list_repartitions',
			# proposer une jointure entre les scrutins et les documents (accusé réception de dépot de liste...)
			'documents_liens'
		),
		'statut'=> array(
			array(
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'statut_textes_instituer' => array(
			'prepa' => 'texte_statut_en_cours_redaction',
			'prop' => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			'refuse' => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'texte_changer_statut' => 'list_elec:texte_changer_statut',
		
	);

	$tables['spip_list_scrutins'] = array(
		'texte_retour' => 'icone_retour',
		'texte_objets' => 'list_scrutin:titre_list_scrutins',
		'texte_objet' => 'list_scrutin:titre_list_scrutin',
		'texte_modifier' => 'list_scrutin:icone_modifier_list_scrutin',
		'texte_creer' => 'list_scrutin:icone_nouveau_list_scrutin',
		'info_aucun_objet'=> 'list_scrutin:info_aucun_list_scrutin',
		'info_1_objet' => 'list_scrutin:info_1_list_scrutin',
		'info_nb_objets' => 'list_scrutin:info_nb_list_scrutins',
		'texte_logo_objet' => 'list_scrutin:titre_logo_list_scrutin',
		'texte_langue_objet' => 'list_scrutin:titre_langue_list_scrutin',
		'type' => 'list_scrutin',
		'principale' => 'oui',
		'table_objet_surnoms' => array('listscrutin'),
		'field'=> array(
			'id_list_scrutin' => "bigint(21) NOT NULL",
			'id_list_operation' => "bigint(21) NOT NULL",
			'recevabilite' => "varchar(255) DEFAULT '' NOT NULL",
			'titre'  => "varchar(255) DEFAULT '' NOT NULL",
			'abrege'  => "varchar(25) DEFAULT '' NOT NULL",
			'inscrits' => "bigint(21) NOT NULL",
			'siege' => "bigint(21) NOT NULL",
			'prime' => "smallint UNSIGNED default 0",
			'seuil' => "decimal(4,2) UNSIGNED default 0.00",
			'repartition' => "enum('moyenne','reste') NOT NULL DEFAULT 'moyenne'",
			'acquisition' => "enum('on','501exp') NOT NULL DEFAULT 'on'",
			'seuil_elegibilite' => "bigint(21) NOT NULL",
			'seuil_inclusion' => "bigint(21) NOT NULL",
			'date_depot' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'date_ouverture' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'date_cloture' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			# les 3 colonnes pour les résultats
			'votants' =>  "bigint(21) NOT NULL",
			'blancs' =>  "bigint(21) NOT NULL",
			'nuls' =>  "bigint(21) NOT NULL",
			'statut' => "varchar(255) DEFAULT '' NOT NULL",
			'lang' => "varchar(10) DEFAULT '' NOT NULL",
			'langue_choisie' => "varchar(3) DEFAULT 'non'",
			'id_trad' => "bigint(21) DEFAULT '0' NOT NULL",
			'maj' => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY" => "id_list_scrutin",
			'KEY' => 'id_list_operation',
		),
		'titre' => "titre AS titre, '' AS lang",
		'date' => "date_cloture",
		'champs_editables' => array(
			'id_list_operation', 'recevabilite', 'titre', 'abrege', 'inscrits', 'siege', 'prime', 'seuil', 'acquisition', 'seuil_elegibilite', 'seuil_inclusion', 'date_depot', 'date_ouverture', 'date_cloture', 'validite', 'votants', 'blancs', 'nuls'
		),
		'champs_versionnes' => array(
			 'id_list_operation', 'recevabilite', 'titre', 'abrege', 'inscrits', 'siege', 'prime', 'seuil', 'acquisition', 'seuil_elegibilite', 'seuil_inclusion', 'date_depot', 'date_ouverture', 'date_cloture', 'validite', 'votants', 'blancs', 'nuls'
		),
		'rechercher_champs' => array(
			'titre' => 8, 'abrege' => 10
		),
		'tables_jointures' => array(
			# proposer une jointure entre les scrutins et les documents (procès verbeaux...)
			'documents_liens'
		),
		'statut'=> array(
			array(
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'statut_textes_instituer' => array(
			'prepa' => 'texte_statut_en_cours_redaction',
			'prop' => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			'refuse' => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		// images du statut supplémentaire 'remplacé' (images placée dans /images)
		// mais l'ajout nécessite une redéclaration de l'ensemble.
		'statut_images' => array(
			'prepa'    => 'puce-preparer-8.png',
			'prop'     => 'puce-proposer-8.png',
			'publie'   => 'puce-publier-8.png',
			'refuse'   => 'puce-refuser-8.png',
			'poubelle' => 'puce-supprimer-8.png',
		),
		'texte_changer_statut' => 'list_scrutin:texte_changer_statut',
	);

	$tables['spip_list_operations'] = array(
		'texte_retour' => 'icone_retour',
		'texte_objets' => 'list_operation:titre_list_operations',
		'texte_objet' => 'list_operation:titre_list_operation',
		'texte_modifier' => 'list_operation:icone_modifier_list_operation',
		'texte_creer' => 'list_operation:icone_nouveau_list_operation',
		'info_aucun_objet'=> 'list_operation:info_aucun_list_operation',
		'info_1_objet' => 'list_operation:info_1_list_operation',
		'info_nb_objets' => 'list_operation:info_nb_list_operations',
		'texte_logo_objet' => 'list_operation:titre_logo_list_operation',
		'texte_langue_objet' => 'list_operation:titre_langue_list_operation',
		'type' => 'list_operation',
		'principale' => 'oui',
		'table_objet_surnoms' => array('listoperation'),
		'field'=> array(
			'id_list_operation' => "bigint(21) NOT NULL",
			'titre'  => "varchar(255) DEFAULT '' NOT NULL",
			'date_debut' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'date_fin' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'statut' => "varchar(255) DEFAULT '' NOT NULL",
			'lang' => "varchar(10) DEFAULT '' NOT NULL",
			'langue_choisie' => "varchar(3) DEFAULT 'non'",
			'id_trad' => "bigint(21) DEFAULT '0' NOT NULL",
			'maj' => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY" => "id_list_operation",
		),
		'titre' => "titre AS titre, '' AS lang",
		'date' => "date_fin",
		'champs_editables' => array(
			'titre', 'date_debut', 'date_fin'
		),
		'champs_versionnes' => array(
			'titre', 'abrege', 'date_debut', 'date_fin'
		),
		'rechercher_champs' => array(
			'titre' => 8
		),
		'tables_jointures' => array(),
		'statut'=> array(
			array(
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'statut_textes_instituer' => array(
			'prepa' => 'texte_statut_en_cours_redaction',
			'prop' => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			'refuse' => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		// images du statut supplémentaire 'remplacé' (images placée dans /images)
		// mais l'ajout nécessite une redéclaration de l'ensemble.
		'statut_images' => array(
			'prepa'    => 'puce-preparer-8.png',
			'prop'     => 'puce-proposer-8.png',
			'publie'   => 'puce-publier-8.png',
			'refuse'   => 'puce-refuser-8.png',
			'poubelle' => 'puce-supprimer-8.png',
		),
		'texte_changer_statut' => 'list_operation:texte_changer_statut',
	);

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @param array $tables
 * @return array
 */
function op_elec_declarer_tables_auxiliaires($tables) {

	$tables['spip_list_elecs_liens'] = array(
		'field' => array(
			'id_list_elec' => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet' => 'varchar(25) DEFAULT "" NOT NULL',
			'id_objet' => 'bigint(21) DEFAULT "0" NOT NULL',
			'role' => 'varchar(30) NOT NULL DEFAULT ""',
			'vu' => 'varchar(6) DEFAULT "non" NOT NULL',
		),
		'key' => array(
			'PRIMARY KEY'       => 'id_list_elec,id_objet,objet,role',
			'KEY id_list_elec'  => 'id_list_elec',
			'KEY objet'         => 'objet',
			'KEY id_objet'      => 'id_objet',
			'KEY role'          => 'role'
		)
	);

	$tables['spip_list_repartitions'] = array(
		'field'=> array(
			'id_list_repartition' => 'bigint(21) NOT NULL AUTO_INCREMENT',
			'id_list_elec' => 'bigint(21) NOT NULL',
			'id_list_scrutin' => 'bigint(21) NOT NULL',
			'pourcentage_des_suffrages' => 'decimal(4,2) UNSIGNED default 0.00',
			'repartition_au_quotient' =>  'bigint(21) NOT NULL',
			'prime' => 'bigint(21) NOT NULL',
			'attribution_moyenne' =>  'bigint(21) NOT NULL',
			'class' => 'varchar(255) NOT NULL DEFAULT ""',
			'specificites' => 'text',
		),
		'key' => array(
			'PRIMARY KEY' => 'id_list_repartition',
			'KEY id_list_elec' => 'id_list_elec',
			'KEY id_list_scrutin' => 'id_list_scrutin',
		),
	);

	return $tables;
}

/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @param array $interfaces
 * @return array
 */
function op_elec_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['list_membres'] = 'list_membres';
	$interfaces['table_des_tables']['list_elecs'] = 'list_elecs';
	$interfaces['table_des_tables']['list_scrutins'] = 'list_scrutins';
	$interfaces['table_des_tables']['list_operations'] = 'list_operations';

	return $interfaces;
}

