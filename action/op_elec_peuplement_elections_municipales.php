<?php
/**
 * Gestion de l'action op_elec_peuplement_elections_municipales
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Op_elec\Action
 */
 
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Action pour donner un exemple d'election municipale,
 * 
 * Se base sur l'exemple donné par un exemple donné par le memento du Ministère de l'Intérieur
 * @link : https://www.interieur.gouv.fr/content/download/120532/966785/file/guide-elections-municipales-et-communautaires-2020-communes-de-1000-habitants-et-plus-maj-09012020.pdf
 *
 * @use charger_fonction
 * @link https://programmer.spip.net/charger_fonction
 * @param  string    $arg
 *  non utilisé.
 * @return void
**/
 
function action_op_elec_peuplement_elections_municipales_dist($arg=null){
	include_spip('inc/autoriser');
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (!autoriser('peuplement', 'operation')) {
		return;
	}

	// On lance le peuplement
	$peuplement = charger_fonction('peuplement_elections_municipales_go','action');
	$erreur = $peuplement();

	// on l'a fait
	spip_log(_T('list_elec_peuplement:log_elections_municipales_out', array(
				'id' => $GLOBALS['auteur_session']['id_auteur'],
				'nom' => $GLOBALS['auteur_session']['nom'],
				'erreur' => $erreur
			)
		),
		'op_elec.' . _LOG_INFO_IMPORTANTE
	);

	return $erreur;
}

/**
 * Fonction de peuplement
 *
 * @use editer_objet
 * @link https://www.spip.net/fr_article5526.html
 * @param  string    $pays
 * @return void
**/

function action_peuplement_elections_municipales_go($arg=null){
	$message_log = array();

		include_spip('action/editer_objet');

	// des opérations électorales... (identifiants = $retrouve_id_list_operation)
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_operations();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_operation'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles);
		foreach ($inserts as $key => $tableaux) {
			$id_list_operation = objet_inserer('list_operation', '', $tableaux);
			$retrouve_id_list_operation[$correspondances[$key]] = $id_list_operation;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_operation',
					'id' => $id_list_operation
				)
			);
		}
	}
	
	// ...composées de scrutins...
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_scrutins();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_scrutin'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} elseif ($cles[$k] == 'id_list_operation') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_operation[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_list_scrutin = objet_inserer('list_scrutin', '', $tableaux);
			$retrouve_id_list_scrutin[$correspondances[$key]] = $id_list_scrutin;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_scrutin',
					'id' => $id_list_scrutin
				)
			);
		}
	}
	
	// ...avec des listes soumises dans chaque scrutin...
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_elecs();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_elec'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} elseif ($cles[$k] == 'id_list_scrutin') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_scrutin[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_list_elec = objet_inserer('list_elec', '', $tableaux);
			$retrouve_id_list_elec[$correspondances[$key]] = $id_list_elec;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_elec',
					'id' => $id_list_elec
				)
			);
		}
	}
	
	// ...avec des membres dans chaque liste...
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_membres();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_membre'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} elseif ($cles[$k] == 'id_list_elec') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_elec[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		set_request('id_auteur',''); // nécessaire pour que l'association à l'auteur courant ne soit pas faite
		foreach ($inserts as $key => $tableaux) {
			$id_list_membre = objet_inserer('list_membre', '', $tableaux);
			$retrouve_id_list_membre[$correspondances[$key]] = $id_list_membre;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_membre',
					'id' => $id_list_membre
				)
			);
		}
	}

	include_spip('action/editer_liens');

	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_auteurs_liens();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_objet') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_membre[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);$liens = array();
		foreach ($inserts as $key => $tableaux) {
			$nb_liens = objet_associer(
				array('auteur' => $tableaux['id_auteur']),
				array('list_membre' => $tableaux['id_objet']),
				# array('role' => $tableaux['role'])
			);
			$liens[] = array(
				'id_auteur' => $tableaux['id_auteur'],
				'objet' => $tableaux['objet'],
				'id_objet' => $tableaux['id_objet'],
				'resultat liaison' => $nb_lien
			);
		}
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'assos' => print_r($liens,TRUE)
				)
			);
			unset($liens);
	}

		// ...avec des liaisons entre les territoires et des objets du plugin list_elec ...

	if (test_plugin_actif('territoires') and in_array('spip_territoires', lire_config('list_elec/objets_ressorts',array()))) {
		$inserts = array(); $correspondances = array();
		list($cles, $valeurs) = donnees_spip_territoires_liens();
		if (is_array($valeurs)) {
			foreach ($valeurs as $v) { // le jeu de données
				$i = array();$objet = $v[2];
				foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
					if ($cles[$k] == 'id_objet' and $objet == 'list_operation') { // et l'on retrouve l'identifiant
						$i[$cles[$k]] = $retrouve_id_list_operation[$valeur];
					} elseif ($cles[$k] == 'id_objet' and $objet == 'list_scrutin'){
						$i[$cles[$k]] = $retrouve_id_list_scrutin[$valeur];
					} else {
						$i[$cles[$k]] = $valeur;
					}
				}
				$inserts[] = $i;
			}
			unset($valeurs,$cles,$i);$liens = array();
			foreach ($inserts as $key => $tableaux) {
				$nb_liens = objet_associer(
					array( 'territoire' => $tableaux['id_territoire'] ),
					array( $tableaux['objet'] => $tableaux['id_objet'] ),
					array( 'role' => $tableaux['role'] )
				);
				$liens[] = array(
					'id_territoire' => $tableaux['id_territoire'],
					'objet' => $tableaux['objet'],
					'id_objet' => $tableaux['id_objet'],
					'resultat liaison' => $nb_lien
				);
			}
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'assos' => print_r($liens,TRUE)
				)
			);
			unset($liens);
		}
	}

	if (is_array($message_log)){
		$message_log = print_r($message_log,true);
	}

	return $message_log;
}

/**
 * Donnees de la table spip_list_operations
 *
 * Les dates de l'opération électorale sont plus larges que celle d'ouverture et de fermeture
 * des scrutins qui la compose. Il s'agit ici d'intéreger un rétropanning des tâches administratives
 * à effectuer pour organiser les scrutins et les finaliser.
 *
 * Le présent exemple prend des dates relatives aux élections de 2020 :
 * - pour date de début de la période la date d'ouverture de la période pendant laquelle
 *   les recettes et les dépenses en vue de l’élection sont comptabilisées 
 *   au compte de campagne soit le 1er septembre 2019 !
 * - pour date de fin de période, celle de la limite de dépôt
 *   des comptes de campagne des listes à la Commission nationale des comptes de campagne
 *   et des financements politiques soit le soit le 22 mai 2020 !
 *
**/
function donnees_spip_list_operations() {

	$cles = array(
		'id_list_operation',
		'titre',
		'date_debut',
		'date_fin',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);

	$valeurs = array(
		array('1', 'Une commune', '2019-09-01 00:00:00', '2020-05-22 18:00:00', 'publie', 'fr', 'non', '0', '2020-11-27 00:00:00'),
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_list_scrutins
 *
 * le nom du fichier Yaml contenant les paramètres pour vérifier la recevabilité de la liste
 * est indiqué dans la colonne 'recevabilite'. Son absence n'empêche en rien le traitement
 * mais n'ouvre aucune vérification de recevabilité.
 *
**/
function donnees_spip_list_scrutins() {

	$cles = array(
		'id_list_scrutin',
		'id_list_operation',
		'recevabilite',
		'titre',
		'abrege',
		'votants',
		'blancs',
		'nuls',
		'inscrits',
		'siege',
		'prime',
		'seuil',
		'repartition',
		'acquisition',
		'seuil_elegibilite',
		'seuil_inclusion',
		'date_depot',
		'date_ouverture',
		'date_cloture',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);
//Cas d’une commune de 2 300 habitants avec un effectif municipal de 19 membres ayant 4 sièges au sein de la communauté de communes dont elle est membre.
	$valeurs = array(
		array('1', '1', 'municipale_exemple', 'Premier tour des élections municipales (exemple)', 'Scrutin 1er tour',
			'951', // votants
			'12', // blancs
			'7', // nuls
			'1352', // inscrits
			'19', // Nombre des membres du conseil municipal, pour une commune de 1 500 à 2 499 habitants
			'10', // prime majoritaire (il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. )
			'5', // seuil de 5% à partir duquel les listes sont admises à la répartition des sièges
			'moyenne', // élection proportionnelle à la plus forte moyenne
			'501exp', // élection acquise à la majorité absolue des suffrages exprimés (50%+1) sinon second tour
			'10', // seuil d'éligibilité permettant de se présenter au second tour (si personne n'a la majorité absolue)
			'5', // seuil permettant aux listes de voir ses membres pouvoir recomposer des listes éligibles 
			'2020-02-27 18:00:00', // Clôture du dépôt de candidature en préfecture ou sous-préfecture
			'2020-03-15 08:00:00', // Ouverture du scrutin
			'2020-03-15 20:00:00', // Fermeture du scrutin
			'publie', 'fr', 'non', '0', '2020-11-27 00:00:00'),
		array('2', '1', 'municipale_exemple', 'Second tour des élections municipales (exemple)', 'Scrutin 2ème tour',
			'940', // votants
			'7', // blancs
			'11', // nuls
			'1354', // inscrits
			'19',
			'10',
			'5',
			'moyenne',
			'on', // acquisition des sièges à l'issue du scrutin du second tour
			'0', // sans objet car acquisition des sièges à l'issue du scrutin
			'0', // sans objet car acquisition des sièges à l'issue du scrutin
			'2020-02-27 18:00:00', 
			'2020-03-22 08:00:00',
			'2020-03-22 20:00:00',
			'publie', 'fr', 'non', '0', '2020-11-27 00:00:00'),
			
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_list_elecs
 *
 * Cas d’une commune de 2 300 habitants avec un effectif municipal de 19 membres ayant 4 sièges au sein de la communauté de communes dont elle est membre. Page 76 du memento précité.
 *
 * Les n° d'id_list_elec retenu sont ceux du tirage au sort,
 * mais la présente d'autres listes dans la base de données invitera le script
 * à affecter, in fine, un autre numéro d'identifiant unique. Il maintiendra néanmoins l'ordre.
 * (debug : si vous placez par erreur des numéros d'identifiant unique similaires,
 * l'algorithme choisira le dernier similaire).
 *
**/
function donnees_spip_list_elecs() {

	$cles = array(
		'id_list_elec',
		'id_list_scrutin',
		'titre',
		'abrege',
		'voix',
		'date',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);
	
	$valeurs = array(
		array('1', '1', 'Liste n°1', 'L1', '321', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '0', '2020-02-27 18:00:00'),
		array('2', '1', 'Liste n°2', 'L2', '81', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '0', '2020-02-27 18:00:00'),
		array('3', '1', 'Liste n°3', 'L3', '120', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '0', '2020-02-27 18:00:00'),
		array('4', '1', 'Liste n°4', 'L4', '410', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '0', '2020-02-27 18:00:00'),

		array('5', '2', 'Liste n°4', 'L4', '418', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('6', '2', 'Liste n°1', 'L1', '316', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('7', '2', 'Liste n°3 ayant repris des candidats de la liste n°2', 'L3 + L2', '206', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
			
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_list_membres
 *
**/
function donnees_spip_list_membres() {


	$cles = array(
		'id_list_membre',
		'id_list_elec',
		'nom',
		'genre',
		'position',
		'groupe',
		'suppl',
		'date',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);

	$valeurs = array(
// Premier tour
		
// liste n°1 
		array('1', '1', 'ANONYMOUS Pierre', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('2', '1', 'ANONYMOUS Henriette', 'femme', '2', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('3', '1', 'ANONYMOUS Philippe', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('4', '1', 'ANONYMOUS Jeanne', 'femme', '4', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('5', '1', 'ANONYMOUS Olivier', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('6', '1', 'ANONYMOUS Anne', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('7', '1', 'ANONYMOUS Frédéric', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('8', '1', 'ANONYMOUS Emilie', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('9', '1', 'ANONYMOUS Arthur', 'homme', '9', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('10', '1', 'ANONYMOUS Fabienne', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('11', '1', 'ANONYMOUS Fabrice', 'homme', '11', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('12', '1', 'ANONYMOUS Marianne', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('13', '1', 'ANONYMOUS Marc', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('14', '1', 'ANONYMOUS Evelyne', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('15', '1', 'ANONYMOUS Antoine', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('16', '1', 'ANONYMOUS Anita', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('17', '1', 'ANONYMOUS Guy', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('18', '1', 'ANONYMOUS Denise', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('19', '1', 'ANONYMOUS Charles', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('20', '1', 'ANONYMOUS Antoinette', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('21', '1', 'ANONYMOUS Antoine', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('22', '1', 'ANONYMOUS Claire', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
// liste n°2 
		array('23', '2', 'ANONYMOUS Karim', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('24', '2', 'ANONYMOUS Fatiha', 'femme', '2', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('25', '2', 'ANONYMOUS Roger', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('26', '2', 'ANONYMOUS Lucienne', 'femme', '4', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('27', '2', 'ANONYMOUS Ivrin', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('28', '2', 'ANONYMOUS Henriette', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('29', '2', 'ANONYMOUS Vincent', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('30', '2', 'ANONYMOUS Ingrid', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('31', '2', 'ANONYMOUS Grégoire', 'homme', '9', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('32', '2', 'ANONYMOUS Chantal', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('33', '2', 'ANONYMOUS Saïd', 'homme', '11', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('34', '2', 'ANONYMOUS Yun', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('35', '2', 'ANONYMOUS Marc', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('36', '2', 'ANONYMOUS Evelyne', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('37', '2', 'ANONYMOUS Antoine', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('38', '2', 'ANONYMOUS Anita', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('39', '2', 'ANONYMOUS Guy', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('40', '2', 'ANONYMOUS Denise', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('41', '2', 'ANONYMOUS Charles', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('42', '2', 'ANONYMOUS Pomme', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('43', '2', 'ANONYMOUS Arnold', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('44', '2', 'ANONYMOUS Saadia', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),

// liste n°3 
		array('45', '3', 'ANONYMOUS Vincent', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('46', '3', 'ANONYMOUS Isabelle', 'femme', '2', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('47', '3', 'ANONYMOUS Olivier', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('48', '3', 'ANONYMOUS Sarah', 'femme', '4', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('49', '3', 'ANONYMOUS Igor', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('50', '3', 'ANONYMOUS Simone', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('51', '3', 'ANONYMOUS Pierre', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('52', '3', 'ANONYMOUS Caroline', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('53', '3', 'ANONYMOUS Marc', 'homme', '9', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('54', '3', 'ANONYMOUS Tu van', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('55', '3', 'ANONYMOUS Hamid', 'homme', '11', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('56', '3', 'ANONYMOUS Aurore', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('57', '3', 'ANONYMOUS Lancelot', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('58', '3', 'ANONYMOUS Marianne', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('59', '3', 'ANONYMOUS Jean', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('60', '3', 'ANONYMOUS Fleur', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('61', '3', 'ANONYMOUS Roger', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('62', '3', 'ANONYMOUS Laetitia', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('63', '3', 'ANONYMOUS Patrick', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('64', '3', 'ANONYMOUS Eve', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('65', '3', 'ANONYMOUS Adam', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('66', '3', 'ANONYMOUS Sabrina', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),

// liste n°4 
		array('67', '4', 'ANONYMOUS Paul', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('68', '4', 'ANONYMOUS Isabelle', 'femme', '2', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('69', '4', 'ANONYMOUS Olivier', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('70', '4', 'ANONYMOUS Anita', 'femme', '4', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('71', '4', 'ANONYMOUS Florent', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('72', '4', 'ANONYMOUS Simone', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('73', '4', 'ANONYMOUS Pierre', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('74', '4', 'ANONYMOUS Caroline', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('75', '4', 'ANONYMOUS Pierre-Emmanuel', 'homme', '9', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('76', '4', 'ANONYMOUS Eglantine', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('77', '4', 'ANONYMOUS Maurice', 'homme', '11', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('78', '4', 'ANONYMOUS Léa', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('79', '4', 'ANONYMOUS Pierre-François', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('80', '4', 'ANONYMOUS Maie-France', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('81', '4', 'ANONYMOUS Antoine', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('82', '4', 'ANONYMOUS Angélique', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('83', '4', 'ANONYMOUS Roger', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('84', '4', 'ANONYMOUS Korotoumou', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('85', '4', 'ANONYMOUS André', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('86', '4', 'ANONYMOUS Anne', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('87', '4', 'ANONYMOUS Joseph', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('88', '4', 'ANONYMOUS Oumoucourssoum', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
# second tour

// la liste n°4 est en tête

		array('89', '5', 'ANONYMOUS Paul', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('90', '5', 'ANONYMOUS Isabelle', 'femme', '2', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('91', '5', 'ANONYMOUS Olivier', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('92', '5', 'ANONYMOUS Anita', 'femme', '4', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('93', '5', 'ANONYMOUS Florent', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('94', '5', 'ANONYMOUS Simone', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('95', '5', 'ANONYMOUS Pierre', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('96', '5', 'ANONYMOUS Caroline', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('97', '5', 'ANONYMOUS Pierre-Emmanuel', 'homme', '9', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('98', '5', 'ANONYMOUS Eglantine', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('99', '5', 'ANONYMOUS Maurice', 'homme', '11', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('100', '5', 'ANONYMOUS Léa', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('101', '5', 'ANONYMOUS Pierre-François', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('102', '5', 'ANONYMOUS Maie-France', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('103', '5', 'ANONYMOUS Antoine', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('104', '5', 'ANONYMOUS Angélique', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('105', '5', 'ANONYMOUS Roger', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('106', '5', 'ANONYMOUS Korotoumou', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('107', '5', 'ANONYMOUS André', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('108', '5', 'ANONYMOUS Anne', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('109', '5', 'ANONYMOUS Joseph', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('110', '5', 'ANONYMOUS Oumoucourssoum', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
			
// liste n°1 est seconde

		array('111', '6', 'ANONYMOUS Pierre', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('112', '6', 'ANONYMOUS Henriette', 'femme', '2', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('113', '6', 'ANONYMOUS Philippe', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('114', '6', 'ANONYMOUS Jeanne', 'femme', '4', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('115', '6', 'ANONYMOUS Olivier', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('116', '6', 'ANONYMOUS Anne', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('117', '6', 'ANONYMOUS Frédéric', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('118', '6', 'ANONYMOUS Emilie', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('119', '6', 'ANONYMOUS Arthur', 'homme', '9', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('120', '6', 'ANONYMOUS Fabienne', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('121', '6', 'ANONYMOUS Fabrice', 'homme', '11', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('122', '6', 'ANONYMOUS Marianne', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('123', '6', 'ANONYMOUS Marc', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('124', '6', 'ANONYMOUS Evelyne', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('125', '6', 'ANONYMOUS Antoine', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('126', '6', 'ANONYMOUS Anita', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('127', '6', 'ANONYMOUS Guy', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('128', '6', 'ANONYMOUS Denise', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('129', '6', 'ANONYMOUS Charles', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('130', '6', 'ANONYMOUS Antoinette', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('131', '6', 'ANONYMOUS Antoine', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('132', '6', 'ANONYMOUS Claire', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),

// la liste n°3 est troisième et récupère des candidats de la liste 2 qui à lus de 5%
		array('133', '7', 'ANONYMOUS Vincent', 'homme', '1', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('134', '7', 'ANONYMOUS Fatiha', 'femme', '2', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('135', '7', 'ANONYMOUS Olivier', 'homme', '3', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('136', '7', 'ANONYMOUS Sarah', 'femme', '4', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('137', '7', 'ANONYMOUS Igor', 'homme', '5', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('138', '7', 'ANONYMOUS Simone', 'femme', '6', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('139', '7', 'ANONYMOUS Pierre', 'homme', '7', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('140', '7', 'ANONYMOUS Caroline', 'femme', '8', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('141', '7', 'ANONYMOUS Marc', 'homme', '9', '', 'conseiller_communautaire', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('142', '7', 'ANONYMOUS Tu van', 'femme', '10', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('143', '7', 'ANONYMOUS Hamid', 'homme', '11', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('144', '7', 'ANONYMOUS Aurore', 'femme', '12', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('145', '7', 'ANONYMOUS Lancelot', 'homme', '13', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('146', '7', 'ANONYMOUS Marianne', 'femme', '14', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('147', '7', 'ANONYMOUS Jean', 'homme', '15', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('148', '7', 'ANONYMOUS Fleur', 'femme', '16', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('149', '7', 'ANONYMOUS Roger', 'homme', '17', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('150', '7', 'ANONYMOUS Laetitia', 'femme', '18', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('151', '7', 'ANONYMOUS Patrick', 'homme', '19', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('152', '7', 'ANONYMOUS Eve', 'femme', '20', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('153', '7', 'ANONYMOUS Adam', 'homme', '21', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
		array('154', '7', 'ANONYMOUS Sabrina', 'femme', '22', '', '', '2020-02-27 18:00:00', 'publie', 'fr', 'non', '','2020-02-27 18:00:00'),
	);
		
	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_auteurs_lien
 *
 * Il est possible d'associer des auteurs aux candidats
**/
function donnees_spip_auteurs_liens() {


	$cles = array(
		'id_auteur',
		'id_objet',
		'objet',
		'role',
	);

	# pour notre exemple, on dit que le webmeste est le premier membre de la première liste
	$valeurs = array(
		array('1', '1', 'list_membre', ''),
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_territoires_liens
 *
 * Le peuplement des territoires peuvent varier d'une base de données à l'autre
 * et aboutir à des identifiants uniques différents. Il convient donc dans le présent script
 * d'interroger la base de données à partir de l'identifiant fixe qu'est le code iso
 * pour connaitre d'identifiant unique numérique correspondant.
 *
**/
function donnees_spip_territoires_liens() {

	$cles = array(
		'id_territoire',
		'id_objet',
		'objet',
		'role',
	);

	# retrouver l'id d'un seul isocode (pour notre exemple, les Hauts-de-France qui est une région avec 5 départements)
	$c = array();
	$c['FR-02001'] = sql_getfetsel('id_territoire', 'spip_territoires', 'iso_territoire="FR-02001"');

/*
	# retrouver les ids des codes iso des territoires concernés par l'opération electorale
	if ($liens = sql_allfetsel('id_territoire,iso_territoire', 'spip_territoires', "iso_territoire IN ('FR-IDF','FR-HDF')")) {
		foreach ($liens as $l) {
			$c[$l['iso_territoire']] = $l['id_territoire'];
		}
	}
*/

	# définir la liaison
	$valeurs = array(
		array($c['FR-02001'], '1', 'list_operation', 'elec_mun'),
	);

	return array($cles, $valeurs);
}