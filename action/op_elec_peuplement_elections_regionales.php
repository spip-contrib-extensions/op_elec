<?php
/**
 * Gestion de l'action op_elec_peuplement_elections_regionales
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\Action
 */
 
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Action pour donner un exemple d'election régionale pour la France.
 * 
 * Se base sur l'exemple donné par le Memento à l'usage du candidat du Ministère de l'Intérieur
 * @link: https://www.interieur.gouv.fr/content/download/88388/686248/file/2015-regionales-memento-candidats.pdf
 *
 * @use charger_fonction
 * @link https://programmer.spip.net/charger_fonction
 * @param  string    $arg
 *  non utilisé.
 * @return void
**/
 
function action_op_elec_peuplement_elections_regionales_dist($arg=null){
	include_spip('inc/autoriser');
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (!autoriser('peuplement', 'operation')) {
		return;
	}

	// On lance le peuplement
	$peuplement = charger_fonction('peuplement_elections_regionales_go','action');
	$erreur = $peuplement();

	// on l'a fait
	spip_log(_T('list_elec_peuplement:log_elections_regionales_out', array(
				'id' => $GLOBALS['auteur_session']['id_auteur'],
				'nom' => $GLOBALS['auteur_session']['nom'],
				'erreur' => $erreur
			)
		),
		'op_elec.' . _LOG_INFO_IMPORTANTE
	);
	
	return $erreur;
}

/**
 * Fonction de peuplement
 *
 * @use editer_objet
 * @link https://www.spip.net/fr_article5526.html
 * @param  string    $pays
 * @return void
**/

function action_peuplement_elections_regionales_go($arg=null){
	$message_log = array();

		include_spip('action/editer_objet');

	// des opérations électorales... (identifiants = $retrouve_id_list_operation)
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_operations();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_operation'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles);
		foreach ($inserts as $key => $tableaux) {
			$id_list_operation = objet_inserer('list_operation', '', $tableaux);
			$retrouve_id_list_operation[$correspondances[$key]] = $id_list_operation;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_operation',
					'id' => $id_list_operation
				)
			);
		}
	}
	
	// ...composées de scrutins...
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_scrutins();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_scrutin'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} elseif ($cles[$k] == 'id_list_operation') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_operation[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_list_scrutin = objet_inserer('list_scrutin', '', $tableaux);
			$retrouve_id_list_scrutin[$correspondances[$key]] = $id_list_scrutin;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_scrutin',
					'id' => $id_list_scrutin
				)
			);
		}
	}
	
	// ...avec des listes soumises dans chaque scrutin...
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_elecs();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_elec'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} elseif ($cles[$k] == 'id_list_scrutin') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_scrutin[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		foreach ($inserts as $key => $tableaux) {
			$id_list_elec = objet_inserer('list_elec', '', $tableaux);
			$retrouve_id_list_elec[$correspondances[$key]] = $id_list_elec;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_elec',
					'id' => $id_list_elec
				)
			);
		}
	}
	
	// ...avec des membres dans chaque liste...
	
	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_list_membres();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_list_membre'){
					$correspondances[] = $valeur;
					$i[$cles[$k]] = '';
				} elseif ($cles[$k] == 'id_list_elec') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_elec[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);
		set_request('id_auteur',''); // nécessaire pour que l'association à l'auteur courant ne soit pas faite
		foreach ($inserts as $key => $tableaux) {
			$id_list_membre = objet_inserer('list_membre', '', $tableaux);
			$retrouve_id_list_membre[$correspondances[$key]] = $id_list_membre;
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'id_objet' => 'id_list_membre',
					'id' => $id_list_membre
				)
			);
		}
	}

	include_spip('action/editer_liens');

	$inserts = array(); $correspondances = array();
	list($cles, $valeurs) = donnees_spip_auteurs_liens();
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) { // le jeu de données
			$i = array();
			foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
				if ($cles[$k] == 'id_objet') { // et l'on retrouve l'identifiant
					$i[$cles[$k]] = $retrouve_id_list_membre[$valeur];
				} else {
					$i[$cles[$k]] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs,$cles,$i);$liens = array();
		foreach ($inserts as $key => $tableaux) {
			$nb_liens = objet_associer(
				array('auteur' => $tableaux['id_auteur']),
				array('list_membre' => $tableaux['id_objet']),
				# array('role' => $tableaux['role'])
			);
			$liens[] = array(
				'id_auteur' => $tableaux['id_auteur'],
				'objet' => $tableaux['objet'],
				'id_objet' => $tableaux['id_objet'],
				'resultat liaison' => $nb_lien
			);
		}
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'assos' => print_r($liens,TRUE)
				)
			);
			unset($liens);
	}

		// ...avec des liaisons entre les territoires et des objets du plugin list_elec ...

	if (test_plugin_actif('territoires') and in_array('spip_territoires', lire_config('list_elec/objets_ressorts',array()))) {
		$inserts = array(); $correspondances = array();
		list($cles, $valeurs) = donnees_spip_territoires_liens();
		if (is_array($valeurs)) {
			foreach ($valeurs as $v) { // le jeu de données
				$i = array();$objet = $v[2];
				foreach ($v as $k => $valeur) { // que l'on décline pour affecter les clés
					if ($cles[$k] == 'id_objet' and $objet == 'list_operation') { // et l'on retrouve l'identifiant
						$i[$cles[$k]] = $retrouve_id_list_operation[$valeur];
					} elseif ($cles[$k] == 'id_objet' and $objet == 'list_scrutin'){
						$i[$cles[$k]] = $retrouve_id_list_scrutin[$valeur];
					} else {
						$i[$cles[$k]] = $valeur;
					}
				}
				$inserts[] = $i;
			}
			unset($valeurs,$cles,$i);$liens = array();
			foreach ($inserts as $key => $tableaux) {
				$nb_liens = objet_associer(
					array( 'territoire' => $tableaux['id_territoire'] ),
					array( $tableaux['objet'] => $tableaux['id_objet'] ),
					array( 'role' => $tableaux['role'] )
				);
				$liens[] = array(
					'id_territoire' => $tableaux['id_territoire'],
					'objet' => $tableaux['objet'],
					'id_objet' => $tableaux['id_objet'],
					'resultat liaison' => $nb_lien
				);
			}
			$message_log[] = _T('list_elec_peuplement:log_insertion', array(
					'assos' => print_r($liens,TRUE)
				)
			);
			unset($liens);
		}
	}

	if (is_array($message_log)){
		$message_log = print_r($message_log,true);
	}

	return $message_log;
}

/**
 * Donnees de la table spip_list_operations
 *
 * Les dates de l'opération électorale sont plus larges que celle d'ouverture et de fermeture
 * des scrutins qui la compose. Il s'agit ici d'intéreger un rétropanning des tâches administratives
 * à effectuer pour organiser les scrutins et les finaliser.
 *
 * Le présent exemple prend des dates relatives aux élections de 2015 :
 * - pour date de début de la période la date d'ouverture de la période pendant laquelle
 *   les recettes et les dépenses en vue de l’élection sont comptabilisées 
 *   au compte de campagne soit le 1er décembre 2014 !
 * - pour date de fin de période, celle de la limite de dépôt
 *   des comptes de campagne des listes à la Commission nationale des comptes de campagne
 *   et des financements politiques soit le 12 février 2016 !
 *
**/
function donnees_spip_list_operations() {

	$cles = array(
		'id_list_operation',
		'titre',
		'date_debut',
		'date_fin',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);

	$valeurs = array(
		array('1', 'Une région avec 5 départements', '2014-12-01 00:00:00', '2016-02-12 18:00:00', 'publie', 'fr', 'non', '0', '2020-11-27 00:00:00'),
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_list_scrutins
 *
 * le nom du fichier Yaml contenant les paramètres pour vérifier la recevabilité de la liste
 * est indiqué dans la colonne 'recevabilite'. Son absence n'empêche en rien le traitement
 * mais n'ouvre aucune vérification de recevabilité.
 *
**/
function donnees_spip_list_scrutins() {

	$cles = array(
		'id_list_scrutin',
		'id_list_operation',
		'recevabilite',
		'titre',
		'abrege',
		'votants',
		'blancs',
		'nuls',
		'inscrits',
		'siege',
		'prime',
		'seuil',
		'repartition',
		'acquisition',
		'seuil_elegibilite',
		'seuil_inclusion',
		'date_depot',
		'date_ouverture',
		'date_cloture',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);

	$valeurs = array(
		array('1', '1', 'regionale_exemple', 'Premier tour des élections régionales (exemple)', 'Scrutin 1er tour',
			'1147101', // votants
			'1000', // blancs
			'253', // nuls
			'2139877', // inscrits
			'125', // sièges à pourvoir
			'32', // prime majoritaire (1/4 des sièges soit 125x0.25 arrondi à l'entier supérieur)
			'5', // seuil de 5% à partir duquel les listes sont admises à la répartition des sièges
			'moyenne', // élection proportionnelle à la plus forte moyenne
			'501exp', // élection acquise à la majorité absolue des suffrages exprimés (50%+1) sinon second tour
			'10', // seuil d'éligibilité permettant de se présenter au second tour (si personne n'a la majorité absolue)
			'5', // seuil permettant aux listes de voir ses membres pouvoir recomposer des listes éligibles 
			'2015-11-09 12:00:00', '2015-12-06 08:00:00', '2015-12-06 20:00:00', 'publie', 'fr', 'non', '0', '2020-11-27 00:00:00'),
		array('2', '1', 'regionale_exemple', 'Second tour des élections régionales (exemple)', 'Scrutin 2ème tour',
			'1237257', // votants
			'1000', // blancs
			'290', // nuls
			'2139534', // inscrits
			'125',
			'32',
			'5',
			'moyenne',
			'on', // acquisition des sièges à l'issue du scrutin du second tour
			'0', // sans objet car acquisition des sièges à l'issue du scrutin
			'0', // sans objet car acquisition des sièges à l'issue du scrutin
			'2015-11-09 12:00:00', '2015-12-06 08:00:00', '2015-12-06 20:00:00', 'publie', 'fr', 'non', '0', '2020-11-27 00:00:00'),
			
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_list_elecs
 *
 * Les n° d'id_list_elec retenu sont ceux du tirage au sort,
 * mais la présente d'autres listes dans la base de données invitera le script
 * à affecter, in fine, un autre numéro d'identifiant unique. Il maintiendra néanmoins l'ordre.
 * (debug : si vous placez par erreur des numéros d'identifiant unique similaires,
 * l'algorithme choisira le dernier similaire).
 *
**/
function donnees_spip_list_elecs() {

	$cles = array(
		'id_list_elec',
		'id_list_scrutin',
		'titre',
		'abrege',
		'voix',
		'date',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);
	
	$valeurs = array(
		array('1', '1', 'Liste n°1', 'L1', '540032', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('2', '1', 'Liste n°2', 'L2', '139273', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('3', '1', 'Liste n°3', 'L3', '262878', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('4', '1', 'Liste n°4', 'L4', '176841', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('5', '1', 'Liste n°5', 'L5', '7096', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),
		array('6', '1', 'Liste n°6', 'L6', '19981', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '0', '2015-11-09 12:00:00'),

		array('7', '2', 'Liste n°1', 'L1', '543059', '2015-12-08 18:00:00', 'publie', 'fr', 'non', '0', '2015-12-08 18:00:00'),
		array('8', '2', 'Liste n°2', 'L2', '145472', '2015-12-08 18:00:00', 'publie', 'fr', 'non', '0', '2015-12-08 18:00:00'),
		array('9', '2', 'Liste n°3', 'L3', '372879', '2015-12-08 18:00:00', 'publie', 'fr', 'non', '0', '2015-12-08 18:00:00'),
		array('10', '2', 'Liste n°4', 'L4', '174847', '2015-12-08 18:00:00', 'publie', 'fr', 'non', '0', '2015-12-08 18:00:00'),
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_list_membres
 *
**/
function donnees_spip_list_membres() {


	$cles = array(
		'id_list_membre',
		'id_list_elec',
		'nom',
		'genre',
		'position',
		'groupe',
		'date',
		'statut',
		'lang',
		'langue_choisie',
		'id_trad',
		'maj'
	);

	$valeurs = array(
// Premier tour
		
// liste n°1 Département D1 15 membres
		array('1', '1', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('2', '1', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('3', '1', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('4', '1', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('5', '1', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('6', '1', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('7', '1', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('8', '1', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('9', '1', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('10', '1', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('11', '1', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('12', '1', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('13', '1', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('14', '1', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('15', '1', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°1 Département D2 25 membres
		array('16', '1', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('17', '1', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('18', '1', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('19', '1', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('20', '1', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('21', '1', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('22', '1', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('23', '1', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('24', '1', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('25', '1', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('26', '1', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('27', '1', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('28', '1', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('29', '1', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('30', '1', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('31', '1', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('32', '1', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('33', '1', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('34', '1', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('35', '1', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('36', '1', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('37', '1', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('38', '1', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('39', '1', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('40', '1', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°1 Département D3 30 membres
		array('41', '1', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('42', '1', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('43', '1', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('44', '1', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('45', '1', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('46', '1', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('47', '1', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('48', '1', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('49', '1', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('50', '1', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('51', '1', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('52', '1', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('53', '1', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('54', '1', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('55', '1', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('56', '1', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('57', '1', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('58', '1', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('59', '1', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('60', '1', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('61', '1', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('62', '1', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('63', '1', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('64', '1', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('65', '1', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('66', '1', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('67', '1', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('68', '1', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('69', '1', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('70', '1', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°1 Département D4 35 membres
		array('71', '1', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('72', '1', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('73', '1', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('74', '1', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('75', '1', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('76', '1', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('77', '1', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('78', '1', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('79', '1', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('80', '1', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('81', '1', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('82', '1', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('83', '1', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('84', '1', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('85', '1', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('86', '1', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('87', '1', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('88', '1', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('89', '1', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('90', '1', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('91', '1', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('92', '1', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('93', '1', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('94', '1', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('95', '1', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('96', '1', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('97', '1', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('98', '1', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('99', '1', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('100', '1', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('101', '1', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('102', '1', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('103', '1', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('104', '1', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('105', '1', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°1 Département D5 28 membres
		array('106', '1', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('107', '1', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('108', '1', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('109', '1', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('110', '1', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('111', '1', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('112', '1', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('113', '1', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('114', '1', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('115', '1', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('116', '1', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('117', '1', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('118', '1', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('119', '1', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('120', '1', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('121', '1', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('122', '1', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('123', '1', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('124', '1', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('125', '1', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('126', '1', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('127', '1', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('128', '1', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('129', '1', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('130', '1', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('131', '1', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('132', '1', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('133', '1', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
				
				
// liste n°2 Département D1 15 membres
		array('134', '2', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('135', '2', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('136', '2', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('137', '2', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('138', '2', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('139', '2', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('140', '2', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('141', '2', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('142', '2', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('143', '2', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('144', '2', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('145', '2', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('146', '2', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('147', '2', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('148', '2', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°2 Département D2 25 membres
		array('149', '2', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('150', '2', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('151', '2', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('152', '2', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('153', '2', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('154', '2', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('155', '2', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('156', '2', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('157', '2', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('158', '2', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('159', '2', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('160', '2', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('161', '2', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('162', '2', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('163', '2', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('164', '2', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('165', '2', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('166', '2', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('167', '2', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('168', '2', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('169', '2', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('170', '2', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('171', '2', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('172', '2', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('173', '2', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D3 30 membres
		array('174', '2', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('175', '2', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('176', '2', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('177', '2', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('178', '2', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('179', '2', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('180', '2', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('181', '2', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('182', '2', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('183', '2', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('184', '2', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('185', '2', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('186', '2', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('187', '2', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('188', '2', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('189', '2', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('190', '2', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('191', '2', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('192', '2', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('193', '2', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('194', '2', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('195', '2', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('196', '2', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('197', '2', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('198', '2', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('199', '2', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('200', '2', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('201', '2', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('202', '2', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('203', '2', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D4 35 membres
		array('204', '2', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('205', '2', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('206', '2', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('207', '2', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('208', '2', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('209', '2', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('210', '2', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('211', '2', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('212', '2', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('213', '2', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('214', '2', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('215', '2', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('216', '2', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('217', '2', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('218', '2', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('219', '2', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('220', '2', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('221', '2', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('222', '2', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('223', '2', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('224', '2', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('225', '2', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('226', '2', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('227', '2', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('228', '2', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('229', '2', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('230', '2', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('231', '2', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('232', '2', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('233', '2', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('234', '2', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('235', '2', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('236', '2', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('237', '2', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('238', '2', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D5 28 membres
		array('239', '2', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('240', '2', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('241', '2', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('242', '2', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('243', '2', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('244', '2', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('245', '2', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('246', '2', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('247', '2', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('248', '2', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('249', '2', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('250', '2', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('251', '2', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('252', '2', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('253', '2', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('254', '2', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('255', '2', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('256', '2', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('257', '2', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('258', '2', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('259', '2', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('260', '2', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('261', '2', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('262', '2', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('263', '2', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('264', '2', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('265', '2', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('266', '2', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
			
// liste n°3 Département D1 15 membres
		array('267', '3', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('268', '3', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('269', '3', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('270', '3', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('271', '3', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('272', '3', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('273', '3', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('274', '3', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('275', '3', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('276', '3', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('277', '3', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('278', '3', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('279', '3', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('280', '3', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('281', '3', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°3 Département D2 25 membres
		array('282', '3', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('283', '3', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('284', '3', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('285', '3', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('286', '3', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('287', '3', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('288', '3', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('289', '3', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('290', '3', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('291', '3', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('292', '3', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('293', '3', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('294', '3', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('295', '3', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('296', '3', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('297', '3', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('298', '3', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('299', '3', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('300', '3', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('301', '3', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('302', '3', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('303', '3', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('304', '3', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('305', '3', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('306', '3', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°3 Département D3 30 membres
		array('307', '3', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('308', '3', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('309', '3', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('310', '3', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('311', '3', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('312', '3', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('313', '3', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('314', '3', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('315', '3', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('316', '3', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('317', '3', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('318', '3', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('319', '3', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('320', '3', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('321', '3', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('322', '3', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('323', '3', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('324', '3', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('325', '3', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('326', '3', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('327', '3', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('328', '3', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('329', '3', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('340', '3', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('341', '3', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('342', '3', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('343', '3', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('344', '3', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('345', '3', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('346', '3', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°3 Département D4 35 membres
		array('347', '3', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('348', '3', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('349', '3', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('350', '3', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('351', '3', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('352', '3', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('353', '3', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('354', '3', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('355', '3', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('356', '3', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('357', '3', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('358', '3', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('359', '3', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('360', '3', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('361', '3', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('362', '3', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('363', '3', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('364', '3', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('365', '3', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('366', '3', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('367', '3', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('368', '3', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('369', '3', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('370', '3', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('371', '3', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('372', '3', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('373', '3', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('374', '3', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('375', '3', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('376', '3', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('377', '3', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('378', '3', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('379', '3', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('380', '3', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('381', '3', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°3 Département D5 28 membres
		array('382', '3', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('383', '3', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('384', '3', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('385', '3', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('386', '3', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('387', '3', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('388', '3', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('389', '3', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('390', '3', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('391', '3', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('392', '3', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('393', '3', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('394', '3', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('395', '3', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('396', '3', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('397', '3', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('398', '3', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('399', '3', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('400', '3', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('401', '3', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('402', '3', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('403', '3', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('404', '3', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('405', '3', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('406', '3', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('407', '3', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('408', '3', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('409', '3', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		
// liste n°4 Département D1 15 membres
		array('410', '4', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('411', '4', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('412', '4', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('413', '4', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('414', '4', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('415', '4', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('416', '4', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('417', '4', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('418', '4', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('419', '4', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('420', '4', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('421', '4', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('422', '4', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('423', '4', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('424', '4', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°4 Département D2 25 membres
		array('425', '4', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('426', '4', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('427', '4', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('428', '4', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('429', '4', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('430', '4', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('431', '4', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('432', '4', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('433', '4', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('434', '4', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('435', '4', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('436', '4', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('437', '4', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('438', '4', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('439', '4', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('440', '4', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('441', '4', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('442', '4', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('443', '4', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('444', '4', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('445', '4', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('446', '4', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('447', '4', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('448', '4', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('449', '4', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°4 Département D3 30 membres
		array('450', '4', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('451', '4', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('452', '4', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('453', '4', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('454', '4', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('455', '4', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('456', '4', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('457', '4', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('458', '4', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('459', '4', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('460', '4', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('461', '4', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('462', '4', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('463', '4', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('464', '4', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('465', '4', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('466', '4', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('467', '4', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('468', '4', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('469', '4', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('470', '4', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('471', '4', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('472', '4', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('473', '4', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('474', '4', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('475', '4', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('476', '4', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('477', '4', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('478', '4', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('479', '4', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°4 Département D4 35 membres
		array('480', '4', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('481', '4', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('482', '4', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('483', '4', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('484', '4', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('485', '4', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('486', '4', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('487', '4', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('488', '4', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('489', '4', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('490', '4', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('491', '4', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('492', '4', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('493', '4', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('494', '4', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('495', '4', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('496', '4', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('497', '4', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('498', '4', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('499', '4', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('500', '4', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('501', '4', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('502', '4', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('503', '4', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('504', '4', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('505', '4', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('506', '4', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('507', '4', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('508', '4', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('509', '4', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('510', '4', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('511', '4', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('512', '4', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('513', '4', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('514', '4', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°4 Département D5 28 membres
		array('515', '4', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('516', '4', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('517', '4', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('518', '4', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('519', '4', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('520', '4', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('521', '4', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('522', '4', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('523', '4', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('524', '4', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('525', '4', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('526', '4', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('527', '4', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('528', '4', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('529', '4', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('530', '4', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('531', '4', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('532', '4', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('533', '4', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('534', '4', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('535', '4', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('536', '4', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('537', '4', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('538', '4', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('539', '4', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('540', '4', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('541', '4', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('542', '4', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),


// liste n°5 Département D1 15 membres
		array('543', '5', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('544', '5', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('545', '5', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('546', '5', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('547', '5', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('548', '5', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('549', '5', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('550', '5', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('551', '5', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('552', '5', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('553', '5', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('554', '5', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('555', '5', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('556', '5', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('557', '5', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°5 Département D2 25 membres
		array('558', '5', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('559', '5', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('560', '5', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('561', '5', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('562', '5', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('563', '5', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('564', '5', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('565', '5', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('566', '5', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('567', '5', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('568', '5', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('569', '5', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('570', '5', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('571', '5', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('572', '5', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('573', '5', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('574', '5', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('575', '5', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('576', '5', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('577', '5', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('578', '5', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('579', '5', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('580', '5', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('581', '5', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('582', '5', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°5 Département D3 30 membres
		array('583', '5', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('584', '5', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('585', '5', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('586', '5', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('587', '5', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('588', '5', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('589', '5', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('590', '5', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('591', '5', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('592', '5', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('593', '5', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('594', '5', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('595', '5', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('596', '5', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('597', '5', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('598', '5', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('599', '5', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('600', '5', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('601', '5', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('602', '5', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('603', '5', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('604', '5', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('605', '5', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('606', '5', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('607', '5', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('608', '5', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('609', '5', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('610', '5', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('611', '5', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('612', '5', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°5 Département D4 35 membres
		array('613', '5', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('614', '5', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('615', '5', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('616', '5', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('617', '5', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('618', '5', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('619', '5', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('620', '5', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('621', '5', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('622', '5', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('623', '5', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('624', '5', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('625', '5', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('626', '5', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('627', '5', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('628', '5', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('629', '5', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('630', '5', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('631', '5', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('632', '5', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('633', '5', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('634', '5', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('635', '5', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('636', '5', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('637', '5', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('638', '5', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('639', '5', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('640', '5', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('641', '5', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('642', '5', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('643', '5', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('644', '5', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('645', '5', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('646', '5', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('647', '5', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°5 Département D5 28 membres
		array('648', '5', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('649', '5', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('650', '5', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('651', '5', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('652', '5', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('653', '5', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('654', '5', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('655', '5', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('656', '5', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('657', '5', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('658', '5', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('659', '5', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('660', '5', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('661', '5', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('662', '5', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('663', '5', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('664', '5', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('665', '5', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('666', '5', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('667', '5', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('668', '5', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('669', '5', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('670', '5', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('671', '5', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('672', '5', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('673', '5', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('674', '5', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('675', '5', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),

	// liste n°6 Département D1 15 membres
		array('676', '6', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('677', '6', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('678', '6', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('679', '6', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('680', '6', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('681', '6', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('682', '6', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('683', '6', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('684', '6', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('685', '6', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('686', '6', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('687', '6', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('688', '6', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('689', '6', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('690', '6', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°6 Département D2 25 membres
		array('691', '6', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('692', '6', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('693', '6', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('694', '6', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('695', '6', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('696', '6', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('697', '6', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('698', '6', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('699', '6', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('701', '6', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('702', '6', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('703', '6', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('704', '6', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('705', '6', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('706', '6', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('707', '6', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('708', '6', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('709', '6', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('710', '6', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('711', '6', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('712', '6', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('713', '6', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('714', '6', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('715', '6', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('716', '6', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°6 Département D3 30 membres
		array('717', '6', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('718', '6', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('719', '6', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('720', '6', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('721', '6', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('722', '6', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('723', '6', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('724', '6', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('725', '6', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('726', '6', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('727', '6', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('728', '6', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('729', '6', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('730', '6', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('731', '6', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('732', '6', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('733', '6', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('734', '6', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('735', '6', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('736', '6', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('737', '6', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('738', '6', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('739', '6', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('740', '6', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('741', '6', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('742', '6', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('743', '6', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('744', '6', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('745', '6', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('746', '6', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°6 Département D4 35 membres
		array('747', '6', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('748', '6', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('749', '6', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('750', '6', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('751', '6', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('752', '6', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('753', '6', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('754', '6', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('755', '6', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('756', '6', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('757', '6', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('758', '6', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('759', '6', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('760', '6', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('761', '6', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('762', '6', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('763', '6', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('764', '6', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('765', '6', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('766', '6', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('767', '6', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('768', '6', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('769', '6', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('770', '6', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('771', '6', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('772', '6', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('773', '6', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('774', '6', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('775', '6', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('776', '6', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('777', '6', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('778', '6', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('779', '6', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('780', '6', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('781', '6', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°6 Département D5 28 membres
		array('782', '6', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('783', '6', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('784', '6', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('785', '6', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('786', '6', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('787', '6', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('788', '6', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('789', '6', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('790', '6', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('791', '6', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('792', '6', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('793', '6', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('794', '6', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('795', '6', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('796', '6', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('797', '6', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('798', '6', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('799', '6', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('800', '6', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('801', '6', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('802', '6', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('803', '6', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('804', '6', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('805', '6', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('806', '6', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('807', '6', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('808', '6', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('809', '6', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),

// liste n°1 (identifiée ici comme 7) Département D1 15 membres
		array('810', '7', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('811', '7', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('812', '7', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('813', '7', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('814', '7', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('815', '7', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('816', '7', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('817', '7', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('818', '7', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('819', '7', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('820', '7', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('821', '7', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('822', '7', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('823', '7', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('824', '7', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°1 Département D2 25 membres
		array('825', '7', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('826', '7', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('827', '7', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('828', '7', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('829', '7', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('830', '7', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('831', '7', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('832', '7', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('833', '7', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('834', '7', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('835', '7', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('836', '7', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('837', '7', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('838', '7', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('839', '7', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('840', '7', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('841', '7', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('842', '7', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('843', '7', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('844', '7', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('845', '7', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('846', '7', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('847', '7', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('848', '7', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('849', '7', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°1 Département D3 30 membres
		array('850', '7', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('851', '7', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('852', '7', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('853', '7', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('854', '7', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('855', '7', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('856', '7', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('857', '7', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('858', '7', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('859', '7', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('860', '7', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('861', '7', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('862', '7', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('863', '7', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('864', '7', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('865', '7', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('866', '7', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('867', '7', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('868', '7', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('869', '7', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('870', '7', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('871', '7', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('872', '7', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('873', '7', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('874', '7', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('875', '7', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('876', '7', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('877', '7', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('878', '7', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('879', '7', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°1 Département D4 35 membres
		array('880', '7', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('881', '7', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('882', '7', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('883', '7', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('884', '7', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('885', '7', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('886', '7', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('887', '7', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('888', '7', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('889', '7', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('890', '7', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('891', '7', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('892', '7', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('893', '7', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('894', '7', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('895', '7', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('896', '7', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('897', '7', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('898', '7', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('899', '7', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('900', '7', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('901', '7', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('902', '7', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('903', '7', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('904', '7', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('905', '7', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('906', '7', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('907', '7', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('908', '7', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('909', '7', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('910', '7', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('911', '7', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('912', '7', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('913', '7', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('914', '7', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D5 28 membres
		array('915', '7', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('916', '7', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('917', '7', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('918', '7', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('919', '7', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('920', '7', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('921', '7', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('922', '7', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('923', '7', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('924', '7', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('925', '7', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('926', '7', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('927', '7', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('928', '7', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('929', '7', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('930', '7', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('931', '7', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('932', '7', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('933', '7', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('934', '7', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('935', '7', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('936', '7', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('937', '7', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('938', '7', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('939', '7', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('940', '7', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('941', '7', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('942', '7', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		
// liste n°2 (identifiée ici comme 8) Département D1 15 membres
		array('943', '8', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('944', '8', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('945', '8', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('946', '8', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('947', '8', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('948', '8', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('949', '8', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('950', '8', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('951', '8', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('952', '8', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('953', '8', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('954', '8', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('955', '8', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('956', '8', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('957', '8', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°2 Département D2 25 membres
		array('958', '8', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('959', '8', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('960', '8', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('961', '8', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('962', '8', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('963', '8', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('964', '8', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('965', '8', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('966', '8', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('967', '8', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('968', '8', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('969', '8', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('970', '8', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('971', '8', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('972', '8', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('973', '8', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('974', '8', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('975', '8', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('976', '8', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('977', '8', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('978', '8', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('979', '8', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('980', '8', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('981', '8', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('982', '8', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D3 30 membres
		array('983', '8', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('984', '8', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('985', '8', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('986', '8', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('987', '8', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('988', '8', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('989', '8', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('990', '8', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('991', '8', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('992', '8', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('993', '8', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('994', '8', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('995', '8', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('996', '8', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('997', '8', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('998', '8', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('999', '8', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1000', '8', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1001', '8', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1002', '8', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1003', '8', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1004', '8', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1005', '8', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1006', '8', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1007', '8', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1008', '8', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1009', '8', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1010', '8', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1011', '8', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1012', '8', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D4 35 membres
		array('1013', '8', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1014', '8', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1015', '8', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1016', '8', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1017', '8', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1018', '8', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1019', '8', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1020', '8', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1021', '8', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1022', '8', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1023', '8', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1024', '8', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1025', '8', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1026', '8', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1027', '8', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1028', '8', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1029', '8', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1030', '8', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1031', '8', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1032', '8', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1033', '8', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1034', '8', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1035', '8', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1036', '8', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1037', '8', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1038', '8', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1039', '8', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1040', '8', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1041', '8', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1042', '8', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1043', '8', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1044', '8', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1045', '8', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1046', '8', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1047', '8', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D5 28 membres
		array('1048', '8', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1049', '8', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1050', '8', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1051', '8', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1052', '8', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1053', '8', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1054', '8', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1055', '8', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1056', '8', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1057', '8', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1058', '8', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1059', '8', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1060', '8', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1061', '8', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1062', '8', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1063', '8', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1064', '8', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1065', '8', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1066', '8', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1067', '8', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1068', '8', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1069', '8', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1070', '8', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1071', '8', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1072', '8', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1073', '8', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1074', '8', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1075', '8', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		
// liste n°3 (identifiée ici comme 9) Département D1 15 membres
		array('1076', '9', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1077', '9', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1078', '9', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1079', '9', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1080', '9', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1081', '9', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1082', '9', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1083', '9', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1084', '9', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1085', '9', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1086', '9', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1087', '9', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1088', '9', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1089', '9', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1090', '9', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°3 Département D2 25 membres
		array('1091', '9', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1092', '9', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1093', '9', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1094', '9', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1095', '9', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1096', '9', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1097', '9', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1098', '9', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1099', '9', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1100', '9', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1101', '9', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1102', '9', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1103', '9', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1104', '9', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1105', '9', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1106', '9', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1107', '9', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1108', '9', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1109', '9', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1110', '9', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1111', '9', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1112', '9', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1113', '9', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1114', '9', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1115', '9', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°3 Département D3 30 membres
		array('1116', '9', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1117', '9', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1118', '9', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1119', '9', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1120', '9', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1121', '9', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1122', '9', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1123', '9', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1124', '9', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1125', '9', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1126', '9', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1127', '9', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1128', '9', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1129', '9', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1130', '9', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1131', '9', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1132', '9', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1133', '9', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1134', '9', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1135', '9', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1136', '9', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1137', '9', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1138', '9', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1139', '9', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1140', '9', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1141', '9', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1142', '9', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1143', '9', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1144', '9', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1145', '9', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°3 Département D4 35 membres
		array('1146', '9', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1147', '9', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1148', '9', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1149', '9', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1150', '9', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1151', '9', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1152', '9', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1153', '9', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1154', '9', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1155', '9', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1156', '9', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1157', '9', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1158', '9', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1159', '9', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1160', '9', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1161', '9', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1162', '9', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1163', '9', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1164', '9', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1165', '9', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1166', '9', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1167', '9', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1168', '9', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1169', '9', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1170', '9', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1171', '9', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1172', '9', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1173', '9', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1174', '9', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1175', '9', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1176', '9', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1177', '9', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1178', '9', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1179', '9', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1180', '9', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°3 Département D5 28 membres
		array('1181', '9', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1182', '9', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1183', '9', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1184', '9', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1185', '9', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1186', '9', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1187', '9', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1188', '9', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1189', '9', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1190', '9', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1191', '9', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1192', '9', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1193', '9', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1194', '9', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1195', '9', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1196', '9', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1197', '9', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1198', '9', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1199', '9', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1200', '9', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1201', '9', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1202', '9', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1203', '9', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1204', '9', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1205', '9', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1206', '9', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1207', '9', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1208', '9', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		
// liste n°4 (identifiée ici comme 10) Département D1 15 membres
		array('1209', '10', 'ANONYMOUS Eric', 'homme', '1', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1210', '10', 'ANONYMOUS Céline', 'femme', '2', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1211', '10', 'ANONYMOUS Pierre', 'homme', '3', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1212', '10', 'ANONYMOUS Marie', 'femme', '4', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1213', '10', 'ANONYMOUS Michel', 'homme', '5', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1214', '10', 'ANONYMOUS Michele', 'femme', '6', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1215', '10', 'ANONYMOUS Bruno', 'homme', '7', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1216', '10', 'ANONYMOUS Saadia', 'femme', '8', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1217', '10', 'ANONYMOUS Abdelâatik', 'homme', '9', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1218', '10', 'ANONYMOUS Sandrine', 'femme', '10', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1219', '10', 'ANONYMOUS Eric', 'homme', '11', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1220', '10', 'ANONYMOUS Céline', 'femme', '12', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1221', '10', 'ANONYMOUS Pierre', 'homme', '13', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1222', '10', 'ANONYMOUS Marie', 'femme', '14', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
		array('1223', '10', 'ANONYMOUS Michel', 'homme', '15', 'D1', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '','2015-11-09 12:00:00'),
// liste n°2 Département D2 25 membres
		array('1224', '10', 'ANONYMOUS Jean-François', 'homme', '1', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1225', '10', 'ANONYMOUS Sylvie', 'femme', '2', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1226', '10', 'ANONYMOUS Franck', 'homme', '3', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1227', '10', 'ANONYMOUS Françoise', 'femme', '4', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1228', '10', 'ANONYMOUS Alain', 'homme', '5', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1229', '10', 'ANONYMOUS Marina', 'femme', '6', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1230', '10', 'ANONYMOUS Sylvain', 'homme', '7', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1231', '10', 'ANONYMOUS Khadija', 'femme', '8', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1232', '10', 'ANONYMOUS Patrick', 'homme', '9', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1233', '10', 'ANONYMOUS Heger', 'femme', '10', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1234', '10', 'ANONYMOUS Vincent', 'homme', '11', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1235', '10', 'ANONYMOUS Véronique', 'femme', '12', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1236', '10', 'ANONYMOUS Yannick', 'homme', '13', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1237', '10', 'ANONYMOUS Françoise', 'femme', '14', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1238', '10', 'ANONYMOUS Saïd', 'homme', '15', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1239', '10', 'ANONYMOUS Marina', 'femme', '16', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1240', '10', 'ANONYMOUS Cyril', 'homme', '17', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1241', '10', 'ANONYMOUS Dominique', 'femme', '18', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1242', '10', 'ANONYMOUS Patrick', 'homme', '19', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1243', '10', 'ANONYMOUS Luciane', 'femme', '20', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1244', '10', 'ANONYMOUS lucien', 'homme', '21', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1245', '10', 'ANONYMOUS Hélène', 'femme', '22', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1246', '10', 'ANONYMOUS Marc', 'homme', '23', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1247', '10', 'ANONYMOUS Marie', 'femme', '24', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1248', '10', 'ANONYMOUS Michel', 'homme', '25', 'D2', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D3 30 membres
		array('1249', '10', 'ANONYMOUS Colette', 'femme', '1', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1250', '10', 'ANONYMOUS Julien', 'homme', '2', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1251', '10', 'ANONYMOUS Manon', 'femme', '3', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1252', '10', 'ANONYMOUS Éric', 'homme', '4', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1253', '10', 'ANONYMOUS Françoise', 'femme', '5', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1254', '10', 'ANONYMOUS Thibaut', 'homme', '6', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1255', '10', 'ANONYMOUS Bénédicte', 'femme', '7', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1256', '10', 'ANONYMOUS Djamel', 'homme', '8', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1257', '10', 'ANONYMOUS Nelly', 'femme', '9', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1258', '10', 'ANONYMOUS Denis', 'homme', '10', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1259', '10', 'ANONYMOUS Florence', 'femme', '11', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1260', '10', 'ANONYMOUS Navid', 'homme', '12', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1261', '10', 'ANONYMOUS Catherine', 'femme', '13', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1262', '10', 'ANONYMOUS Thibaud', 'homme', '14', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1263', '10', 'ANONYMOUS Emilie', 'femme', '15', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1264', '10', 'ANONYMOUS Jean-Marc', 'homme', '16', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1265', '10', 'ANONYMOUS Valérie', 'femme', '17', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1266', '10', 'ANONYMOUS Dimitri', 'homme', '18', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1267', '10', 'ANONYMOUS Florence', 'femme', '19', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1268', '10', 'ANONYMOUS Luc', 'homme', '20', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1269', '10', 'ANONYMOUS Moinamina', 'femme', '21', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1270', '10', 'ANONYMOUS Patric', 'homme', '22', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1271', '10', 'ANONYMOUS Manon', 'femme', '23', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1272', '10', 'ANONYMOUS Éric', 'homme', '24', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1273', '10', 'ANONYMOUS Françoise', 'femme', '25', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1274', '10', 'ANONYMOUS Thibaut', 'homme', '26', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1275', '10', 'ANONYMOUS Bénédicte', 'femme', '27', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1276', '10', 'ANONYMOUS Djamel', 'homme', '28', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1277', '10', 'ANONYMOUS Nelly', 'femme', '29', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1278', '10', 'ANONYMOUS Denis', 'homme', '30', 'D3', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D4 35 membres
		array('1279', '10', 'ANONYMOUS Rachel', 'femme', '1', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1280', '10', 'ANONYMOUS Kamel', 'homme', '2', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1281', '10', 'ANONYMOUS Vanessa', 'femme', '3', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1282', '10', 'ANONYMOUS Aissa', 'homme', '4', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1283', '10', 'ANONYMOUS Hélène', 'femme', '5', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1284', '10', 'ANONYMOUS Marc', 'homme', '6', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1285', '10', 'ANONYMOUS Françoise', 'femme', '7', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1286', '10', 'ANONYMOUS Julien', 'homme', '8', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1287', '10', 'ANONYMOUS Tatiana', 'femme', '9', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1288', '10', 'ANONYMOUS Saliou', 'homme', '10', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1289', '10', 'ANONYMOUS Nadia', 'femme', '11', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1290', '10', 'ANONYMOUS Clément', 'homme', '12', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1291', '10', 'ANONYMOUS Françoise', 'femme', '13', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1292', '10', 'ANONYMOUS Salim', 'homme', '14', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1293', '10', 'ANONYMOUS Catherine', 'femme', '15', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1294', '10', 'ANONYMOUS Hicham', 'homme', '16', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1295', '10', 'ANONYMOUS Monique', 'femme', '17', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1296', '10', 'ANONYMOUS Oumar', 'homme', '18', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1297', '10', 'ANONYMOUS Caroline', 'femme', '19', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1298', '10', 'ANONYMOUS Rodolphe', 'homme', '20', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1299', '10', 'ANONYMOUS Christine', 'femme', '21', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1300', '10', 'ANONYMOUS Jonathan', 'homme', '22', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1301', '10', 'ANONYMOUS Aurélie', 'femme', '23', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1302', '10', 'ANONYMOUS Helmut', 'homme', '24', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1303', '10', 'ANONYMOUS Hulya', 'femme', '25', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1304', '10', 'ANONYMOUS Mehdi', 'homme', '26', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1305', '10', 'ANONYMOUS Sandra', 'femme', '27', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1306', '10', 'ANONYMOUS Marc', 'homme', '28', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1307', '10', 'ANONYMOUS Jacqueline', 'femme', '29', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1308', '10', 'ANONYMOUS Richard', 'homme', '30', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1309', '10', 'ANONYMOUS Rachel', 'femme', '31', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1310', '10', 'ANONYMOUS Kamel', 'homme', '32', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1311', '10', 'ANONYMOUS Vanessa', 'femme', '33', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1312', '10', 'ANONYMOUS Aissa', 'homme', '34', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1313', '10', 'ANONYMOUS Hélène', 'femme', '35', 'D4', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
// liste n°2 Département D5 28 membres
		array('1314', '10', 'ANONYMOUS Clémentine', 'femme', '1', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1315', '10', 'ANONYMOUS Taylan', 'homme', '2', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1316', '10', 'ANONYMOUS Jacqueline', 'femme', '3', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1317', '10', 'ANONYMOUS Didier', 'homme', '4', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1318', '10', 'ANONYMOUS Chantal', 'femme', '5', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1319', '10', 'ANONYMOUS Gildas', 'homme', '6', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1320', '10', 'ANONYMOUS Wassila', 'femme', '7', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1321', '10', 'ANONYMOUS Bally', 'homme', '8', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1322', '10', 'ANONYMOUS Amina', 'femme', '9', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1323', '10', 'ANONYMOUS Thomas', 'homme', '10', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1324', '10', 'ANONYMOUS Catherine', 'femme', '11', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1325', '10', 'ANONYMOUS Frédéric', 'homme', '12', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1326', '10', 'ANONYMOUS Lydia', 'femme', '13', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1327', '10', 'ANONYMOUS Laurent', 'homme', '14', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1328', '10', 'ANONYMOUS Capucine', 'femme', '15', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1329', '10', 'ANONYMOUS José', 'homme', '16', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1330', '10', 'ANONYMOUS Akoua', 'femme', '17', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1331', '10', 'ANONYMOUS Bertrand', 'homme', '18', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1332', '10', 'ANONYMOUS Sofia', 'femme', '19', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1333', '10', 'ANONYMOUS Matthias', 'homme', '20', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1334', '10', 'ANONYMOUS Clémentine', 'femme', '21', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1335', '10', 'ANONYMOUS Taylan', 'homme', '22', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1336', '10', 'ANONYMOUS Jacqueline', 'femme', '23', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1337', '10', 'ANONYMOUS Didier', 'homme', '24', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1338', '10', 'ANONYMOUS Chantal', 'femme', '25', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1339', '10', 'ANONYMOUS Gildas', 'homme', '26', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1340', '10', 'ANONYMOUS Wassila', 'femme', '27', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
		array('1341', '10', 'ANONYMOUS Bally', 'homme', '28', 'D5', '2015-11-09 12:00:00', 'publie', 'fr', 'non', '', '2015-11-09 12:00:00'),
	);
		
	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_auteurs_lien
 *
 * Il est possible d'associer des auteurs aux candidats
**/
function donnees_spip_auteurs_liens() {


	$cles = array(
		'id_auteur',
		'id_objet',
		'objet',
		'role',
	);

	# pour notre exemple, on dit que le webmeste est le premier membre de la première liste
	$valeurs = array(
		array('1', '1', 'list_membre', ''),
	);

	return array($cles, $valeurs);
}


/**
 * Donnees de la table spip_territoires_liens
 *
 * Le peuplement des territoires peuvent varier d'une base de données à l'autre
 * et aboutir à des identifiants uniques différents. Il convient donc dans le présent script
 * d'interroger la base de données à partir de l'identifiant fixe qu'est le code iso
 * pour connaitre d'identifiant unique numérique correspondant.
 *
**/
function donnees_spip_territoires_liens() {

	$cles = array(
		'id_territoire',
		'id_objet',
		'objet',
		'role',
	);

	# retrouver l'id d'un seul isocode (pour notre exemple, les Hauts-de-France qui est une région avec 5 départements)
	$c = array();
	$c['FR-HDF'] = sql_getfetsel('id_territoire', 'spip_territoires', 'iso_territoire="FR-HDF"');

/*
	# retrouver les ids des codes iso des territoires concernés par l'opération electorale
	if ($liens = sql_allfetsel('id_territoire,iso_territoire', 'spip_territoires', "iso_territoire IN ('FR-IDF','FR-HDF')")) {
		foreach ($liens as $l) {
			$c[$l['iso_territoire']] = $l['id_territoire'];
		}
	}
*/

	# définir la liaison
	$valeurs = array(
		array($c['FR-HDF'], '1', 'list_operation', 'elec_regio'),
	);

	return array($cles, $valeurs);
}