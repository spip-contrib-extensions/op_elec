<?php
/**
 * prévoit l'installation et la désinstallation du plugin Listes electorales
 *
 * @plugin     Opérations électorales
 * prefix      op_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction d'installation et MAJ plugin
 * @param string $nom_meta_base_version
 *   Nom de la meta d'installation du plugin
 * @param float $version_cible
 *  Version vers laquelle mettre à jour
 */

function op_elec_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	$maj['create'] = array(
		# créer les tables
		array('maj_tables', array(
			'spip_list_membres',     // les candidats
			'spip_list_elecs',       // leurs listes
			'spip_list_elecs_liens', // pour associer des listes à des objets (regroupement de famille politique...)
			'spip_list_scrutins',    // le scrutin (un premier tour, un second...)
			'spip_list_repartitions',// la répartition des sièges à l'issue du scrutin
			'spip_list_operations',  // l'opération electorale (qui peut regrouper plusieurs scrutins ou voir se succéder plusieurs scrutins)
			)
		),
		# activer l’interface d’ajout de documents (fichiers bureautiques, images, multimédia, etc.) pour les listes électorales
		array('meta_documents_op_elecs'),
		array('plugins_pour_op_elec'),
		array('ecrire_config','op_elec/objets_candidats', array('spip_auteurs')),
		);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Desinstallation
 *
 * @param string $nom_meta_base_version
 */

function op_elec_vider_tables($nom_meta_base_version) {

	# vider les tables du plugin
	sql_drop_table("spip_list_membres");
	sql_drop_table("spip_list_elecs");
	sql_drop_table("spip_list_elecs_liens");
	sql_drop_table("spip_list_scrutins");
	sql_drop_table("spip_list_repartitions");
	sql_drop_table("spip_list_operations");

	# vider les liaisons vers les membres déclarés dans la configuration
	include_spip('inc/config');
	$objets_candidats = lire_config('op_elec/objets_candidats');
	$tables = lister_tables_spip();
	$tables = array_flip($tables);
	foreach ($objets_candidats as $table){
		// si la table de liaison existe, on supprime la liaison
		if ($table and in_array($table . "_liens",$tables)) {
			spip_log(_T('list_elec:cfg_objets_candidats') . ' ' . _T('list_elec:cfg_objets_desintallation', array(
					'out' => $table . "_liens")
				),
				'op_elec.' . _LOG_INFO_IMPORTANTE
			);
			sql_delete($table . "_liens", "objet='list_membre'");
		}
	}

	# vider les liaisons vers les objets de ressorts déclarées dans la configuration
	include_spip('inc/config');
	$objets_ressorts = lire_config('op_elec/objets_ressorts');
	$tables = lister_tables_spip();
	$tables = array_flip($tables);
	foreach ($objets_ressorts as $table){
		// si la table de liaison existe, on supprime la liaison
		if ($table and in_array($table . "_liens",$tables)) {
			spip_log(_T('list_elec:cfg_objets_ressorts') . ' ' . _T('list_elec:cfg_objets_desintallation', array(
					'out' => $table . "_liens")
				),
				'op_elec.' . _LOG_INFO_IMPORTANTE
			);
			sql_delete($table . "_liens", "objet='list_elec'");
			sql_delete($table . "_liens", "objet='list_scrutin'");
			sql_delete($table . "_liens", "objet='list_operation'");
		}
	}

	effacer_meta($nom_meta_base_version);

	# Retirer les listes de candidatures (list_elecs) et les scrutins (list_scrutins) de la liste des objets où téléverser des documents
	$objets = @array_filter(explode(',', $GLOBALS['meta']['documents_objets']));
	if (in_array('spip_list_elecs', $objets) OR in_array('spip_list_scrutins', $objets)) {
		$objets = array_diff($objets, array('spip_list_elecs','spip_list_scrutins'));
		ecrire_meta('documents_objets', implode(',', $objets));
	}

}

/**
 * Fonction privée : ajouter les listes de candidatures (list_elecs) et les scrutins (list_scrutins) à la liste des objets
 * pouvant recevoir des documents
 *
 * @return void
 */
function meta_documents_op_elecs() {
	if (!in_array('spip_list_elecs', $e = explode(',', $GLOBALS['meta']['documents_objets']))) {
		$e = array_filter($e);
		$e[] = 'spip_list_elecs';
		$e[] = 'spip_list_scrutins';
		ecrire_meta('documents_objets', implode(',', $e));
	}
}

/**
 * Fonction privée : permettre l'interaction avec des plugins utiles
 *
 * @return void
 */
function plugins_pour_op_elec() {
	# définir à l'installation quelques associations utilisées

	if (test_plugin_actif('territoires')) {
			ecrire_config('op_elec/objets_ressorts', array('spip_territoires'));
	}
}
