<?php
/**
 * Ce fichier contient l'ensemble des fonctions implémentant l'API du plugin.
 *
 * @plugin     Opérations électorales
 * @prefix     op_elec
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Op_elec\API
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction de calcul des résultats électoraux
 *
 * @param  array    $scrutin
 *   tableau avec les informations nécessaires relatives au scrutin pour calculer la répartition des sièges
 *                    ['siege']
 *                    ['prime']
 *                    ['seuil']
 *                    ['repartition']
 *                    ['acquisition']
 *                    ['seuil_elegibilite']
 *                    ['seuil_inclusion']
 * @param  array    $toutes
 *   tableau avec les informations nécessaires relatives aux listes pour calculer la répartition des sièges
 *                    ['id_list_elec'], l'identifiant unique de la liste.
 *                    ['voix']
 * @return array $listes
 *   Description de la répartition des sièges entre les listes.
 *   Chaque liste est décrite par un tableau dont la clé est l'identifiant unique de la liste.
**/
function op_elec_calcul_des_resultats_dist($scrutin, $toutes){

	if ($scrutin){

		$sieges_restants_a_pourvoir = $scrutin['siege'] - $scrutin['prime'];

		if ($toutes){

			# combien de listes a-t-on et quel est le total des suffrages exprimés ?

			$listes = array();
			$nb_listes = count($toutes);
			$suffrages_exprimes = 0;
			foreach ($toutes as $key => $value) {
				$suffrages_exprimes = $suffrages_exprimes + $value['voix'];
				$listes[$value['id_list_elec']] = array('voix' => $value['voix']);
			}

			# quelle est la liste arrivée en tête ?

			$resultats = array(); 
			$en_tête = array_keys($listes, max($listes)); 
			
			# quel est le pourcentage des suffrages pour chaque liste ?

			foreach ($toutes as $key => $value) {
				$listes[$value['id_list_elec']]['pourcentage_des_suffrages'] = round($value['voix'] / ($suffrages_exprimes/100),2);
				if ($scrutin['acquisition'] == '501exp'){
					if ($listes[$value['id_list_elec']]['pourcentage_des_suffrages'] > 50) {
						$listes[$value['id_list_elec']]['class'] = 'list_1';
						$acquis = 'oui';
					} elseif ($listes[$value['id_list_elec']]['pourcentage_des_suffrages'] >= intval($scrutin['seuil_elegibilite'])) {
						$listes[$value['id_list_elec']]['class'] = 'list_eligible';
					} elseif ($listes[$value['id_list_elec']]['pourcentage_des_suffrages'] >= intval($scrutin['seuil_inclusion'])) {
						$listes[$value['id_list_elec']]['class'] = 'list_inclue';
					}
				}
			}

			# si l'élection n'est pas acquise, on retourne le tableau sans répartition de siège

			if ($scrutin['acquisition'] == '501exp' and $acquis != 'oui') {
				return $listes;
			}

			# la prime majoritaire est donnée à la liste arrivée en tête

			if (count($en_tête)>1) {
					// il faut aller chercher la règle qui s'applique en cas d'égalité
			} else {
				$listes[$en_tête[0]]['prime'] = $scrutin['prime'];
				$listes[$en_tête[0]]['class'] = 'list_siege';
			}
			
			# répartition des siège aux listes (ayant dépassé le seuil si spécifié)

			$retenues = array();
			foreach ($toutes as $key => $value) {
				$listes[$value['id_list_elec']]['pourcentage_des_suffrages'] = round($value['voix'] / ($suffrages_exprimes/100),2);
				if ($scrutin['seuil']) {
					if ($listes[$value['id_list_elec']]['pourcentage_des_suffrages'] >= $scrutin['seuil']) {
						$retenues[$value['id_list_elec']] = array('voix' => $value['voix']);
					}
				} else {
					$retenues[$value['id_list_elec']] = array('voix' => $value['voix']);
				}
				if ($scrutin['acquisition'] == '501exp') {
					if ($listes[$value['id_list_elec']]['pourcentage_des_suffrages'] > 50) {
						$retenues[$value['id_list_elec']] = array('acquis' => 'oui', 'class' => 'list_1');
					} elseif (isset($scrutin['seuil_elegibilite']) 
						and $listes[$value['id_list_elec']]['pourcentage_des_suffrages'] >= intval($scrutin['seuil_elegibilite'])) {
						$retenues[$value['id_list_elec']] = array('class' => 'list_eligible');
					} elseif (isset($scrutin['seuil_elegibilite']) 
						and $listes[$value['id_list_elec']]['pourcentage_des_suffrages'] >= intval($scrutin['seuil_inclusion'])) {
						$retenues[$value['id_list_elec']] = array('class' => 'list_incluable');
					}
				}
			}

			# attribution des sièges aux listes ayant dépassées le seuil

			if ($sieges_restants_a_pourvoir) {
				# repartition au quotient électoral
				$quotient_electoral = round($suffrages_exprimes / $sieges_restants_a_pourvoir, 2);
				foreach ($retenues as $key => $value) {
					$listes[$key]['repartition_au_quotient'] =  floor($value['voix'] / $quotient_electoral);
					$sieges_restants_a_pourvoir = $sieges_restants_a_pourvoir - $listes[$key]['repartition_au_quotient'];
					$retenues[$key]['siege'] = $listes[$key]['repartition_au_quotient'];
					$listes[$key]['class'] = 'list_siege';
				}

				if ($sieges_restants_a_pourvoir) {
					# attribution à la plus forte moyenne
					if ($scrutin['repartition'] == 'moyenne') {
						$sieges_pourvus = 0;
    					while ($sieges_pourvus < $sieges_restants_a_pourvoir){
							$plus_forte = plus_forte_moyenne($retenues);
							if (count($plus_forte)>1) {
								// il faut aller cherche qui a le plus de candidats
								$nb_candidats = array();
								foreach($plus_forte as $cle => $id_list_elec){
									$nb_candidats[$id_list_elec] = sql_countsel('spip_list_membres', array(
											"id_list_elec=$id_list_elec",
											"statut='publie'"
										)
									);
								}
								$plus_candidats = array_keys($nb_candidats,max($nb_candidats));
								// s'il y a toujours égalité, il y aura tirage au sort
								if (count($plus_candidats)>1) {
									foreach($plus_candidats as $cle => $id_list_elec){
										$ancien = unserialize($listes[$id_list_elec]['specificites']);
										if (!is_array($ancien)) {
											$ancien = array();
										}
										$ancien = array_merge($ancien, array('tirage_au_sort' => 1));
										$listes[$id_list_elec]['specificites'] = serialize($ancien);
									}
									$sieges_pourvus++;
									$retenues[$id_list_elec]['siege']++;
									$listes[$id_list_elec]['class'] = 'list_siege';
								}
								else {
									$listes[$plus_candidats[0]]['attribution_moyenne']++;
									$ancien = unserialize($listes[$id_list_elec]['specificites']);
									if (!is_array($ancien)) {
										$ancien = array();
									}
									$ancien = array_merge($ancien, array('candidats' => 1));
									$listes[$id_list_elec]['specificites'] = serialize($ancien);
									$sieges_pourvus++;
									$retenues[$plus_candidats[0]]['siege']++;
									$listes[$plus_candidats[0]]['class'] = 'list_siege';
								}
							} else {
								$listes[$plus_forte[0]]['attribution_moyenne']++;
								$sieges_pourvus++;
								$retenues[$plus_forte[0]]['siege']++;
								$listes[$plus_forte[0]]['class'] = 'list_siege';
							}
						} // fin du while, boucle affectant les sièges restants
					}
				}
			}
		}
	}

	// construire les saisies des voix qui se sont portées sur les listes
	return $listes;
	//return array ('message_ok' => "<pre>".print_r($listes,true)."</pre>");
}

function plus_forte_moyenne($retenues){
    $moyenne = array();
	foreach($retenues as $key => $value){
		$moyenne[$key] = $value['voix'] / ($value['siege'] + 1) ;
	}
	$plus_forte = array_keys($moyenne,max($moyenne)); 
	spip_log(_T('list_scrutin:log_info_forte_moyenne', array(
				'moyenne' => print_r($moyenne,TRUE)
			)
		),
		'op_elec.' . _LOG_INFO_IMPORTANTE
	);
	return $plus_forte;
}