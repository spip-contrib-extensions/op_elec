<?php
/**
 * Définit les autorisations du plugin Listes électorales
 *
 * @plugin     Opérations électorales
 * prefix      op_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\
 */
 
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function op_elec_autoriser() {
}

// -----------------
// Objet Membre (list_membres)

// voir l'objet éditorial ’list_membre’
function autoriser_listmembre_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// creer l'objet éditorial ’list_membre’
function autoriser_listmembre_creer_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// modifier l'objet éditorial ’list_membre’
function autoriser_listmembre_modifier_dist($faire, $type, $id, $qui, $opt) {
	# le rattachement du membre à sa liste est modifiable par tous les auteurs sauf les visiteurs.
	if ($opt['parentalite'] == 'oui') {
		if ($qui['statut'] != '6forum') {
			return TRUE;
		} else {
			return FALSE;
		}
	}	return true;
}

// créer l'objet éditorial ’list_membre’ dans une ’list_elec’
function autoriser_listelec_creerlistmembredans_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'list_membre', '', $qui, $opt);
}

// -----------------
// Objet Liste électorale (list_elecs)

// voir l'article ’list_elecs’ dans la liste du menu Edition
function autoriser_listelecs_menu_dist($faire, $type, $id, $qui, $opt) {
	 return true;
}

// voir le bouton d'accès rapide pour créer une liste électorale (une ’list_elec’)
function autoriser_listeleccreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// voir l'objet éditorial ’list_elec’
function autoriser_listelec_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// modifier l'objet éditorial ’list_elec’
function autoriser_listelec_modifier_dist($faire, $type, $id, $qui, $opt) {
	# le rattachement de la liste à son scrutin doit être modifié avec précaution.
	if ($opt['parentalite'] == 'oui') {
		if ($qui['statut'] == '0minirezo') {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	return true;
}

// creer l'objet éditorial ’list_elec’
function autoriser_listelec_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// créer l'objet éditorial ’list_elec’ dans une ’list_scrutin’
function autoriser_listscrutin_creerlistelecdans_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'list_elec', '', $qui, $opt);
}

// -----------------
// Objet Scrutin (list_scrutins)

// voir l'objet éditorial ’list_scrutin’
function autoriser_listscrutin_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// creer l'objet éditorial ’list_scrutin’
function autoriser_listscrutin_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// modifier l'objet éditorial ’list_scrutin’
function autoriser_listscrutin_modifier_dist($faire, $type, $id, $qui, $opt) {
	# le rattachement d’un scrutin à une opération électorale doit être modifié avec précaution.
	if ($opt['parentalite'] == 'oui') {
		if ($qui['statut'] == '0minirezo') {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	return true;
}

// créer l'objet éditorial ’list_scrutin’ dans une ’list_operation’
function autoriser_listoperation_creerlistscrutindans_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'list_scrutin', '', $qui, $opt);
}


// -----------------
// Objet Opération électorale (list_operations)

// voir l'article ’list_operations’ dans la liste du menu Edition
function autoriser_listoperations_menu_dist($faire, $type, $id, $qui, $opt) {
	 return true;
}

// voir le bouton d'accès rapide pour créer une opération électorale (une ’list_operation’)
function autoriser_listoperationcreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// voir l'objet éditorial ’list_operation’
function autoriser_listoperation_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// creer l'objet éditorial ’list_operation’
function autoriser_listoperation_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// modifier l'objet éditorial ’list_operation’
function autoriser_listoperation_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// -----------------
// Autorisation de créer des résultats

// creer l'objet éditorial ’list_calcul’, c'est-à-dire calculer les résultats après dépouillement
function autoriser_listcalcul_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// -----------------
// Autorisation de peuplement

// autoriser la mise en place d'exemple d'opérations électorales par un peuplement de la base de données
function autoriser_operation_peuplement_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo'));
}
