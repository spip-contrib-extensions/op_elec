<?php

/**
 * Dérivation des pipelines par le plugin Listes électorales
 * 
 * @plugin     Opérations électorales
 * prefix      op_elec
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL v3
 * @package    SPIP\Op_elec\
 */

if (!defined('_ECRIRE_INC_VERSION'))
	return;

/**
 * Compléter, in fine, le contenu d'un objet
 *
 * Affiche tardivement un contenu sur la vue d'un objet,
 * notamment après les boutons (dont celui d'ajout de document).
 * Pour l'objet List_scrutin, ajoute un bouton de calcul des résultats
 *
 * @pipeline afficher_complement_objet
 *
 * @param array $flux
 *     Données du pipeline
 * @return array
 *     Données du pipeline
 */
function op_elec_afficher_complement_objet($flux) {
	$bouton = '';
	# proposer un bouton pour calculer les résultats d'un scrutin

	if ($flux['args']['type'] == 'list_scrutin') {
		if (autoriser('creer','list_calcul')) {
			include_spip('inc/presentation'); // pour l'appel de la fonction icone_verticale.
			include_spip('inc/filtres'); // pour l'appel de la fonction generer_url_ecrire.
			$bouton .= icone_verticale(_T('list_scrutin:icone_creer_resultat_scrutin'), generer_url_ecrire('list_calcul_edit', "id_list_scrutin=" . $flux['args']['id']), "list_calcul-24.svg", "", 'left') . "<br class='nettoyeur' />";
			$flux['data'].= $bouton;
		}
	}
	return $flux;
}

/**
 * Ajouter des vues (listes) d'enfants sur un objet
 *
 * @pipeline affiche_enfants
 *
 * @param array $flux
 *     Données du pipeline
 * @return array
 *     Données du pipeline
**/
function op_elec_affiche_enfants($flux) {

	if ($e = trouver_objet_exec($flux['args']['exec'])
	  AND ( in_array($e['type'], array('list_elec', 'list_operation', 'list_scrutin')) )
	  AND $e['edition'] == false) {
		  
		$id_objet = $flux['args']['id_objet'];
		$bouton = '';
		
		switch ($e['type']) {
			case 'list_elec':
				if (autoriser('creerlist_membredans','list_elec', $id_objet)) {
					include_spip('inc/presentation'); // pour l'appel de la fonction icone_verticale.
					include_spip('inc/filtres'); // pour l'appel de la fonction generer_url_ecrire.
					$bouton .= icone_verticale(_T('list_membre:icone_creer_list_membre'), generer_url_ecrire('list_membre_edit', "id_list_elec=$id_objet"), "list_membre-24.svg", "new", 'right') . "<br class='nettoyeur' />";
				}
				# le contenu de la liste affiche ses membres, inutile de les afficher à nouveau
				break;	

			case 'list_scrutin':
				# s'il n'y a pas de résultats calculés à afficher par liste, on affiche les listes
				if (sql_countsel("spip_list_repartitions", "id_list_scrutin=$id_objet") == 0) {
					if (autoriser('creerlist_elecdans','list_scrutin', $id_objet)) {
						include_spip('inc/presentation'); // pour l'appel de la fonction icone_verticale.
						include_spip('inc/filtres'); // pour l'appel de la fonction generer_url_ecrire.
						$bouton .= icone_verticale(_T('list_elec:icone_creer_list_elec'), generer_url_ecrire('list_elec_edit', "id_list_scrutin=$id_objet"), "list_elec-24.svg", "new", 'right') . "<br class='nettoyeur' />";
					}
					$flux['data'] .= recuperer_fond(
					'prive/objets/liste/list_elecs',
						array(
							'id_list_scrutin' => $id_objet,
							'masquer_scrutin' => 'oui',
							'masquer_abrege' => 'oui'
						),
						array('ajax' => $id_objet) // précision nécessaire pour que les tris en ajax de la liste se fassent
					);
					$flux['data'] .= $bouton;
				}
				break;

			case 'list_operation':
				if (autoriser('creerlist_scrutindans','list_operation', $id_objet)) {
					include_spip('inc/presentation'); // pour l'appel de la fonction icone_verticale.
					include_spip('inc/filtres'); // pour l'appel de la fonction generer_url_ecrire.
					$bouton .= icone_verticale(_T('list_scrutin:icone_creer_list_scrutin'), generer_url_ecrire('list_scrutin_edit', "id_list_operation=$id_objet"), "list_scrutin-24.svg", "new", 'right') . "<br class='nettoyeur' />";
				}
				$flux['data'] .= recuperer_fond(
					'prive/objets/liste/list_scrutins',
					array(
						'id_list_operation' => $id_objet,
						'masquer_operation' => 'oui',
						'masquer_abrege' => 'oui'
					),
					array('ajax' => $id_objet) // précision nécessaire pour que les tris en ajax de la liste se fassent
				);
				$flux['data'] .= $bouton;
				break;	

		}
	}
	
	return $flux;
}

/**
 * Ajout de contenu, ex ante, sur les vues des objets éditoriaux,
 *
 * notamment des formulaires de liaisons entre objets
 * et, pour l'objet list_elec, le calcul de recevabilité. 
 *
 * @pipeline affiche_milieu
 *
 * @use charger_fonction
 * @link https://programmer.spip.net/charger_fonction
 * @param array $flux d'entrée du pipeline
 * @return array $flux éventuellement modifié
**/
function op_elec_affiche_milieu($flux) {
	$texte = "";
	$e = trouver_objet_exec($flux['args']['exec']);

	# proposer un tableau de l'analyse de la recevabilité de la liste
	if ($e['type'] == 'list_elec' AND !$e['edition']) {
		// quel est le fichier Yaml de recevabilité pour les listes soumises au scrutin ?
		$tab = sql_fetsel('scrutin.recevabilite, liste.id_list_scrutin', 'spip_list_elecs AS liste LEFT JOIN spip_list_scrutins AS scrutin ON liste.id_list_scrutin=scrutin.id_list_scrutin', 'id_list_elec=' . intval($flux['args'][$e['id_table_objet']]));
		if ($tab){
			// on charge et active la fonction d'analyse
			$analyse = charger_fonction('analyse_de_recevabilite','recevabilites');
			$texte .= $analyse(
				$tab['recevabilite'],
				intval($flux['args'][$e['id_table_objet']]),
				$tab['id_list_scrutin']
			);
		} else {
			// $texte .= "<p>on n'a pas recevabilite et id_list_scrutin</p>";
		}
	}

	// associer un objet sur le list_membre (exemple : un auteur)
	if ($e['type'] == 'list_membre' AND !$e['edition']) {
		// déterminer l'objet souhaité
		$objets_candidats = lire_config('op_elec/objets_candidats');
		$tables = lister_tables_spip();
		$tables = array_flip($tables);
		// des objets associables...
		if ($objets_candidats) {
			foreach ($objets_candidats as $table){
				// ... avec une table d'association ...
				if ($table and in_array($table . "_liens",$tables)) {
					// ... dont on a l'objet ...
					$objet = objet_type($table);
					// ... et le nom de la table sans spip_
					$objets = array_search($table, $tables);
					// .. et pour lequel il faudra afficher le formulaire d'association.
					$texte .= recuperer_fond('prive/objets/editer/liens', array(
							'table_source' => $objets,
							'objet' => 'list_membre',
							'id_objet' => $flux['args'][$e['id_table_objet']],
							'editable' => autoriser('associer'.$objets,$e['type'],$e['id_objet'])?'oui':'non'
						)
					);
				}
			}
		}
	}

	// associer un objet sur le list_scrutin (exemple : un territoire)
	if ($e['type'] == 'list_scrutin' AND !$e['edition']) {
		// déterminer l'objet souhaité
		$objets_ressorts = lire_config('op_elec/objets_ressorts');
		$tables = lister_tables_spip();
		$tables = array_flip($tables);
		// des objets associables...
		if ($objets_ressorts) {
			foreach ($objets_ressorts as $table){
				// ... avec une table d'association ...
				if ($table and in_array($table . "_liens",$tables)) {
					// ... dont on a l'objet ...
					$objet = objet_type($table);
					// ... et le nom de la table sans spip_
					$objets = array_search($table, $tables);
					// .. et pour lequel il faudra afficher le formulaire d'association.
					$texte .= recuperer_fond('prive/objets/editer/liens', array(
							'table_source' => $objets,
							'objet' => 'list_scrutin',
							'id_objet' => $flux['args'][$e['id_table_objet']],
							'editable' => autoriser('associer'.$objets,$e['type'],$e['id_objet'])?'oui':'non'
						)
					);
				}
			}
		}
	}

	
		// associer un objet sur le list_operation (exemple : un territoire. Il peut y avoir des scrutins par départements et une opération sur toutes une région. L'association du scrutin sera donc différente de l'association de l'opération.)
	if ($e['type'] == 'list_operation' AND !$e['edition']) {
		// déterminer l'objet souhaité
		$objets_ressorts = lire_config('op_elec/objets_ressorts');
		$tables = lister_tables_spip();
		$tables = array_flip($tables);
		// des objets associables...
		if ($objets_ressorts) {
			foreach ($objets_ressorts as $table){
				// ... avec une table d'association ...
				if ($table and in_array($table . "_liens",$tables)) {
					// ... dont on a l'objet ...
					$objet = objet_type($table);
					// ... et le nom de la table sans spip_
					$objets = array_search($table, $tables);
					// .. et pour lequel il faudra afficher le formulaire d'association.
					$texte .= recuperer_fond('prive/objets/editer/liens', array(
							'table_source' => $objets,
							'objet' => 'list_operation',
							'id_objet' => $flux['args'][$e['id_table_objet']],
							'editable' => autoriser('associer'.$objets,$e['type'],$e['id_objet'])?'oui':'non'
						)
					);
				}
			}
		}
	}
	
	if ($texte) {
		if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
			$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
		else
			$flux['data'] .= $texte;
	}

	return $flux;
}

/**
 * Ajouter des liens de navigation transversale en relation avec le contenu affiché,
 *
 * Ce pipeline affiche_droite permet d’ajouter du contenu dans la colonne « droite »
 * qui n’est d’ailleurs pas forcément à droite :
 * puisque l'affichage est en fonction des préférences et de la langue de l’utilisateur.
 *
 * @pipeline affiche_droite
 *
 * @param array $flux d'entrée du pipeline
 * @exemple : ['args'][exec] => auteurs
 *                    [type-page] => auteurs
 *                    [lang] => fr
 *                    [espace_prive] => 1
 *            ['data'] => ce qui sera affiché
 *
 * @return array $flux éventuellement modifié
 */
function op_elec_affiche_droite($flux) {

	$texte = "";
	$objets_ressorts = lire_config('op_elec/objets_ressorts');
	$objet = objet_type($flux['args']['exec']);
	$table_objet = table_objet($flux['args']['exec']);// sans spip_
	$id_table_objet = id_table_objet($flux['args']['exec']);

	// lorsque l'on regarde un objet associé, indiquer les élements
	if ($objets_ressorts and in_array('spip_'.$table_objet, $objets_ressorts) and ($objet != $table_objet)) {
		if (($id_objet = intval(_request($id_table_objet))) and !(in_array($objet, array('territoire')))) {
			include_spip('action/editer_liens');
			$elements = array('list_scrutin','list_operation');
			foreach ($elements as $element) {
				$liens = objet_trouver_liens(array($objet => $id_objet), array($element => '*'));
				if ($liens) {
					$in = array();
					foreach ($liens as $key => $value) {
						$in[$key] = $value['id_objet'];
					}
					$texte .= recuperer_fond("prive/squelettes/inclure/inc-droite-${element}s-lies", array(
							"maliste" => $in
						)
					);
				}
			}
		}
	}

	
	if ($texte) {
		$flux['data'] .= $texte;
	}
	
	return $flux;
}
